#include "containers_impl.h"
#include "worker_impl.h"
#include <nmf_config.h>

#include <echmetsyscomp.h>
#include <limits>
#include <new>

#define _STRINGIFY(input) #input
#define ERROR_CODE_CASE(erCase) case RetCode::erCase: return _STRINGIFY(erCase)
#define INIT_FAIL_CASE(erCase) case InitializationFailure::erCase: return _STRINGIFY(erCase)
#define SIM_FAIL_CASE(erCase) case SimulationFailure::erCase: return _STRINGIFY(erCase)
#define START_FAIL_CASE(erCase) case StartFailure::erCase: return _STRINGIFY(erCase)

namespace NMF {

RetCode ECHMET_CC changeStepper(const Stepper stepper, Worker *&worker)
{
	Worker *newWorker{nullptr};

	auto wrkInternal = dynamic_cast<WorkerInternal *>(worker);
	if (wrkInternal == nullptr)
		return RetCode::E_INVALID_ARGUMENT;

	auto tRet = makeWorker(newWorker, stepper, wrkInternal->handler(), wrkInternal->handlerContext());
	if (tRet != RetCode::OK)
		return tRet;

	static_cast<WorkerInternal *>(newWorker)->moveState(wrkInternal);

	worker->destroy();

	worker = newWorker;
	return RetCode::OK;
}

const char * ECHMET_CC initFailToString(const InitializationFailure fail)
{
	switch (fail) {
		INIT_FAIL_CASE(NUM_CELLS);
		INIT_FAIL_CASE(CAPILLARY_LENGTH);
		INIT_FAIL_CASE(INVALID_CONSTITUENT);
		INIT_FAIL_CASE(INVALID_COMPLEXATION);
		INIT_FAIL_CASE(DUPLICIT_CONSTITUENT);
		INIT_FAIL_CASE(SYSTEM_TOO_LARGE);
		INIT_FAIL_CASE(PROFILES);
		INIT_FAIL_CASE(COMPOSITION_UNSPECIFIED);
		INIT_FAIL_CASE(NO_CONSTITUENTS);
		INIT_FAIL_CASE(MISMATCHING_PROFILES);
		INIT_FAIL_CASE(FIRST_LAST_CELL);
	default:
		return "Unknown initialization failure";
	}
}

RetCode ECHMET_CC makeProfiles(const ECHMET::RealVec *const *concentrations, const size_t numConstituents, Profiles *&profiles)
{
	if (concentrations == nullptr || numConstituents < 1)
		return RetCode::E_INVALID_ARGUMENT;


	const size_t NCells = concentrations[0]->size();
	if (numConstituents >= size_t(std::numeric_limits<int32_t>::max()) ||
	    NCells >= size_t(std::numeric_limits<int32_t>::max()))
		return RetCode::E_INVALID_ARGUMENT;

	/* Check that all vectors have the same number of elements */
	for (size_t idx{1}; idx < numConstituents; idx++) {
		if (concentrations[idx]->size() != NCells)
			return RetCode::E_INVALID_ARGUMENT;
	}

	auto raw = new (std::nothrow) double[numConstituents * NCells];
	if (raw == nullptr)
		return RetCode::E_NO_MEMORY;

	/* Profile is column-major ordered,
	 * each column represents a single cell with total constituent concentrations
	 */
	for (size_t cell{0}; cell < NCells; cell++) {
		for (size_t row{0}; row < numConstituents; row++)
			raw[cell * numConstituents + row] = concentrations[row]->elem(cell);
	}

	profiles = new ProfilesImpl{static_cast<int32_t>(numConstituents),
				    static_cast<int32_t>(NCells),
				    raw};

	return RetCode::OK;
}

RetCode ECHMET_CC makeWorker(Worker *&worker, const Stepper stepper, const EventHandlerFunc handler, void *context)
{
	const auto simd = ECHMET::cpuSupportedSIMD();

	if (!simd.SSE2)
		return RetCode::E_CPU_UNSUPPORTED;

	switch (stepper) {
	case Stepper::RKCK54:
		if (simd.AVX512.F)
			worker = new (std::nothrow) WorkerImpl<Stepper::RKCK54, InstructionSet::AVX512>{handler, context};
		else if (simd.FMA3 && simd.AVX2)
			worker = new (std::nothrow) WorkerImpl<Stepper::RKCK54, InstructionSet::FMA3>{handler, context};
		else if (simd.AVX)
			worker = new (std::nothrow) WorkerImpl<Stepper::RKCK54, InstructionSet::AVX>{handler, context};
		else
			worker = new (std::nothrow) WorkerImpl<Stepper::RKCK54, InstructionSet::SSE2>{handler, context};
		break;
	case Stepper::ABM4:
		if (simd.AVX512.F)
			worker = new (std::nothrow) WorkerImpl<Stepper::ABM4, InstructionSet::AVX512>{handler, context};
		else if (simd.FMA3 && simd.AVX2)
			worker = new (std::nothrow) WorkerImpl<Stepper::ABM4, InstructionSet::FMA3>{handler, context};
		else if (simd.AVX)
			worker = new (std::nothrow) WorkerImpl<Stepper::ABM4, InstructionSet::AVX>{handler, context};
		else
			worker = new (std::nothrow) WorkerImpl<Stepper::ABM4, InstructionSet::SSE2>{handler, context};
		break;
	default:
		worker = nullptr;
		return RetCode::E_INVALID_ARGUMENT;
	}

	if (worker == nullptr)
		return RetCode::E_NO_MEMORY;

	return RetCode::OK;
}

const char * ECHMET_CC NMFerrorToString(const RetCode err)
{
	switch (err) {
		ERROR_CODE_CASE(OK);
		ERROR_CODE_CASE(E_NO_MEMORY);
		ERROR_CODE_CASE(E_INVALID_ARGUMENT);
		ERROR_CODE_CASE(E_NOT_IMPLEMENTED);
		ERROR_CODE_CASE(E_WORKER_NOT_INITIALIZED);
		ERROR_CODE_CASE(DONE);
		ERROR_CODE_CASE(E_RUNTIME_ERROR);
		ERROR_CODE_CASE(E_CPU_UNSUPPORTED);
		ERROR_CODE_CASE(E_BUSY);
		ERROR_CODE_CASE(E_CANNOT_ZERO_STEP);
		ERROR_CODE_CASE(NOT_RUNNING);
	default:
		return "Unknown error";
	}
}

void ECHMET_CC releaseInitialization(Initialization &init)
{
	for (size_t idx{0}; idx < init.composition->size(); idx++)
		ECHMET::SysComp::releaseInConstituent((*init.composition)[idx]);
	init.composition->destroy();

	if (init.profiles != nullptr)
		init.profiles->destroy();
}

void ECHMET_CC releaseProfileSlice(ProfileSlice &slice)
{
	if (slice.name != nullptr)
		slice.name->destroy();

	if (slice.concentrations != nullptr) {
		for (size_t idx{0}; idx < slice.concentrations->size(); idx++) {
			auto item = (*slice.concentrations)[idx];
			if (item.name != nullptr)
				item.name->destroy();
		}
		slice.concentrations->destroy();
	}
}

const char * ECHMET_CC simFailToString(const SimulationFailure fail)
{
	switch (fail) {
		SIM_FAIL_CASE(CANNOT_ADJUST_DT);
		SIM_FAIL_CASE(INTERNAL);
		SIM_FAIL_CASE(CANNOT_SOLVE_EQUILIBRIUM);
		SIM_FAIL_CASE(CANNOT_CORRECT_MOBILITIES);
	default:
		return "Unknown error";
	}
}

const char * ECHMET_CC startFailToString(const StartFailure fail)
{
	switch (fail) {
		START_FAIL_CASE(T_STOP);
		START_FAIL_CASE(DT);
		START_FAIL_CASE(TOLERANCE);
		START_FAIL_CASE(UPDATE_INTERVAL);
		START_FAIL_CASE(FIRST_LAST_CELL);
	default:
		return "Unknown error";
	}
}

const char * ECHMET_CC versionString()
{
	return _STRINGIFY(LIBNMF_VERSION_MAJOR) "."
	       _STRINGIFY(LIBNMF_VERSION_MINOR) "."
	       _STRINGIFY(LIBNMF_VERSION_PATCH);
}

} // namespace NMF
