#include "worker_util.h"

#include "containers_impl.h"
#include "itemmap_impl.h"
#include "reporter_context.h"
#include "simulation/simulation_context.h"

#include <cassert>
#include <cstring>
#include <memory>

#define USE_ECHMET_CONTAINERS
#include <containers/echmetvec_p.h>

#define CTXMAP(map) ctx.map##Map->m_map
#define ITEM(key, type, value) { key, Item{DataType::type, value} }

namespace NMF {

template <typename T>
static
T & getRef(std::map<std::string, Item> &map, const std::string &key)
{
	assert(map.find(key) != map.end());

	auto &item = map[key];

	return *static_cast<T *>(item.data);
}
#define MGET(type, k) getRef<type>(map, k)

template <typename T>
static
void set(std::map<std::string, Item> &map, const std::string &key, const T &value)
{
	assert(map.find(key) != map.end());

	auto &item = map[key];
	assert(typeMatches<T>(item.type));

	*static_cast<T *>(item.data) = value;
}
#define MSET(k,v) set(map, k, v)

static
auto ionPropsCtxReleaser(ECHMET::IonProps::ComputationContext *p)
{
	p->destroy();
}

static
void fillInitMap(std::map<std::string, Item> &map, const SimulationContext &ctx)
{
	MSET(NMF_K_IU_NUM_CELLS, ctx.NCells);
	MSET(NMF_K_IU_NUM_CONSTITUENTS, ctx.NCO);
	MSET(NMF_K_IU_CFTYPE, ctx.constantForce);
	MSET(NMF_K_IU_DX, ctx.dx);
	MSET(NMF_K_IU_FIRST_CELL, ctx.firstCell);
	MSET(NMF_K_IU_LAST_CELL, ctx.lastCell);
	MSET(NMF_K_IU_VOLTAGE, ctx.voltage);
	MSET(NMF_K_IU_CURRENT, ctx.current);

	auto &names = MGET(StringsImpl, NMF_K_IU_CONSTITUENT_NAMES);
	for (size_t idx{0}; idx < ctx.chemSystem.constituents->size(); idx++) {
		const auto &ctuent = ctx.chemSystem.constituents->at(idx);
		names.append(ctuent->name->c_str());
	}

	/* TODO: Use more efficient copying func */
	{
		/* Conductivity */
		auto &cond = MGET(ProfilesImpl, NMF_K_IU_COND_PROFILE);
		std::memcpy(cond.m_data,
			    ctx.conductivityRawArray,
			    ctx.NCells * sizeof(double)
			   );
	}
	{
		/* pH */
		auto &pH = MGET(ProfilesImpl, NMF_K_IU_PH_PROFILE);
		auto &data = pH.m_data;
		#pragma omp parallel for schedule(static)
		for (int32_t idx = 0; idx < ctx.NCells; idx++) {
			const auto &calcProps = ctx.calcPropsVec[idx];
			data[idx] = ECHMET::IonProps::calculatepH_direct(calcProps.ionicConcentrations->elem(0),
									 ctx.correctDH ? calcProps.ionicStrength : 0.0);
		}

	}
	{
		/* Concentration profiles */
		auto &profs = MGET(ProfilesImpl, NMF_K_IU_PROFILES);
		std::memcpy(profs.m_data,
			    ctx.totalConcentrations.data(),
			    ctx.totalConcentrations.size() * sizeof(double)
			    );
	}
}

static
void fillUpdateMap(std::map<std::string, Item> &map, const SimulationContext &ctx)
{
	MSET(NMF_K_IU_NUM_CONSTITUENTS, ctx.NCO);
	MSET(NMF_K_IU_CURRENT, ctx.current);
	MSET(NMF_K_U_ITER_AVG_TIME, ctx.avgTimePerIter);
	MSET(NMF_K_U_DT, ctx.dt);
	MSET(NMF_K_U_TIME, ctx.t);
	MSET(NMF_K_IU_VOLTAGE, ctx.voltage);
	MSET(NMF_K_IU_CFTYPE, ctx.constantForce);
	MSET(NMF_K_IU_DX, ctx.dx);
	MSET(NMF_K_IU_NUM_CELLS, ctx.NCells);
	MSET(NMF_K_IU_FIRST_CELL, ctx.firstCell);
	MSET(NMF_K_IU_LAST_CELL, ctx.lastCell);

	/* TODO: Use more efficient copying func */
	{
		/* Conductivity */
		auto &cond = MGET(ProfilesImpl, NMF_K_IU_COND_PROFILE);
		std::memcpy(cond.m_data,
			    ctx.conductivityRawArray,
			    ctx.NCells * sizeof(double)
			   );
	}
	{
		/* pH */
		auto &pH = MGET(ProfilesImpl, NMF_K_IU_PH_PROFILE);
		auto &data = pH.m_data;
		#pragma omp parallel for schedule(static)
		for (int32_t idx = 0; idx < ctx.NCells; idx++) {
			const auto &calcProps = ctx.calcPropsVec[idx];
			data[idx] = ECHMET::IonProps::calculatepH_direct(calcProps.ionicConcentrations->elem(0),
									 ctx.correctDH ? calcProps.ionicStrength : 0.0);
		}

	}
	{
		/* Concentration profiles */
		auto &profs = MGET(ProfilesImpl, NMF_K_IU_PROFILES);
		std::memcpy(profs.m_data,
			    ctx.totalConcentrations.data(),
			    ctx.totalConcentrations.size() * sizeof(double)
			    );
	}
}

template <typename T>
static
void deleteItem(void *ptr)
{
	delete static_cast<T *>(ptr);
}

static
void destroyReporterMap(ItemMapImpl *map)
{
	for (auto &entry : map->m_map) {
		auto &item = entry.second;

		if (item.data == nullptr)
			continue;

		switch (item.type) {
		case DataType::DOUBLE_VEC:
			static_cast<ECHMET::RealVec *>(item.data)->destroy();
			break;
		case DataType::STRING:
			static_cast<ECHMET::FixedString *>(item.data)->destroy();
			break;
		case DataType::PROFILES:
			static_cast<ProfilesImpl *>(item.data)->destroy();
			break;
		case DataType::STRINGS:
			static_cast<StringsImpl *>(item.data)->destroy();
			break;
		case DataType::BOOL:
			deleteItem<bool>(item.data);
			break;
		case DataType::INT32:
			deleteItem<int32_t>(item.data);
			break;
		case DataType::DOUBLE:
			deleteItem<double>(item.data);
			break;
		case DataType::SIM_FAIL:
			deleteItem<SimulationFailure>(item.data);
			break;
		case DataType::CF_TYPE:
			deleteItem<ConstantForce>(item.data);
			break;
		default:
			break;
		}
	}
}

void destroyReporterContext(ReporterContext &ctx)
{
	destroyReporterMap(ctx.initMap.get());
	destroyReporterMap(ctx.startMap.get());
	destroyReporterMap(ctx.pauseMap.get());
	destroyReporterMap(ctx.termMap.get());
	destroyReporterMap(ctx.updateMap.get());
}

RetCode fillProfileSlice(const ProfileType type, const ECHMET::FixedString *name, const double x, SimulationContext &ctx, ProfileSlice &slice)
{
	using ECHMET::EnumOps::operator|;

	static const auto opts = ECHMET::IonProps::ComputationContext::defaultOptions() | ECHMET::IonProps::ComputationContext::DISABLE_THREAD_SAFETY;

	slice.name = nullptr;
	slice.concentrations = nullptr;

	const OMPSize cell = ctx.NCells * x / ctx.capillaryLength;
	if (cell < 0 || cell >= ctx.NCells)
		return RetCode::E_INVALID_ARGUMENT;

	auto ionPropsCtx = std::unique_ptr<ECHMET::IonProps::ComputationContext, decltype(&ionPropsCtxReleaser)>(
				ECHMET::IonProps::makeComputationContext(ctx.chemSystem, opts),
				ionPropsCtxReleaser);

	if (ionPropsCtx == nullptr)
		return RetCode::E_NO_MEMORY;

	slice.name = ECHMET::createFixedString(name->c_str());
	slice.x = x;
	slice.cell = cell;
	slice.type = type;

	auto &calcProps = ctx.calcPropsVec[cell];

	switch (type) {
	case ProfileType::CONDUCTIVITY:
	{
		slice.value = ECHMET::IonProps::calculateConductivity(ionPropsCtx.get(), calcProps);
		break;
	}
	case ProfileType::PH:
		slice.value = ECHMET::IonProps::calculatepH(ionPropsCtx.get(), ctx.corrs, calcProps);
		break;
	case ProfileType::CONCENTRATION:
	{
		size_t idx{};
		const ECHMET::SysComp::Constituent *ctuent{nullptr};
		for (; idx < ctx.chemSystem.constituents->size(); idx++) {
			ctuent = ctx.chemSystem.constituents->at(idx);
			if (*ctuent->name == *name)
				break;
		}
		if (idx == ctx.chemSystem.constituents->size())
			return RetCode::E_INVALID_ARGUMENT;

		slice.effectiveMobility = ctx.effectiveMobilities[cell * ctx.NCO + idx];
		/* Why can't we just read ctx.totalConcentrations here?
		 * Total concentrations get updated after each computation step but the
		 * ionic concentrations do not, leaving them one step behind.
		 * You can verify the correctness of the simulation by reading concentrations
		 * from ctx.totalConcentrations and observe the difference between the sum of
		 * ionic concentrations and value from totalConcentrations get smaller
		 * with decreasing dt.
		 * slice.value = ctx.totalConcentrations[cell * ctx.NCO + idx];
		 */
		slice.value = 0.0;

		auto concentrations = ECHMET::createECHMETVec<IFConcPair, false>(0);
		for (idx = 0; idx < ctuent->ionicForms->size(); idx++) {
			const auto iF = ctuent->ionicForms->at(idx);

			IFConcPair p{};
			p.name = ECHMET::createFixedString(iF->name->c_str());

			size_t icIdx{0};
			ctx.chemSystem.ionicConcentrationsByName->at(icIdx, iF->name->c_str());
			p.concentration = calcProps.ionicConcentrations->at(icIdx);
			slice.value += p.concentration;

			concentrations->push_back(p);
		}
		slice.concentrations = concentrations;
	}
	}

	return RetCode::OK;
}

void initReporterContext(ReporterContext &ctx)
{
	ctx.initMap = std::unique_ptr<ItemMapImpl>(new ItemMapImpl{});
	ctx.startMap = std::unique_ptr<ItemMapImpl>(new ItemMapImpl{});
	ctx.pauseMap = std::unique_ptr<ItemMapImpl>(new ItemMapImpl{});
	ctx.termMap = std::unique_ptr<ItemMapImpl>(new ItemMapImpl{});
	ctx.updateMap = std::unique_ptr<ItemMapImpl>(new ItemMapImpl{});

	CTXMAP(init).insert(ITEM(NMF_K_IU_CONSTITUENT_NAMES, STRINGS, new StringsImpl{}));
	CTXMAP(init).insert(ITEM(NMF_K_IU_NUM_CELLS, INT32, new int32_t{}));
	CTXMAP(init).insert(ITEM(NMF_K_IU_NUM_CONSTITUENTS, INT32, new int32_t{}));
	CTXMAP(init).insert(ITEM(NMF_K_IU_CFTYPE, CF_TYPE, new ConstantForce{}));
	CTXMAP(init).insert(ITEM(NMF_K_IU_DX, DOUBLE, new double{}));
	CTXMAP(init).insert(ITEM(NMF_K_IU_COND_PROFILE, PROFILES, nullptr)); /* Must be initialized later */
	CTXMAP(init).insert(ITEM(NMF_K_IU_PH_PROFILE, PROFILES, nullptr)); /* Must be initialized later */
	CTXMAP(init).insert(ITEM(NMF_K_IU_PROFILES, PROFILES, nullptr)); /* Must be initialized later */
	CTXMAP(init).insert(ITEM(NMF_K_IU_FIRST_CELL, INT32, new int32_t{}));
	CTXMAP(init).insert(ITEM(NMF_K_IU_LAST_CELL, INT32, new int32_t{}));
	CTXMAP(init).insert(ITEM(NMF_K_IU_CURRENT, DOUBLE, new double{}));
	CTXMAP(init).insert(ITEM(NMF_K_IU_VOLTAGE, DOUBLE, new double{}));

	CTXMAP(update).insert(ITEM(NMF_K_IU_CURRENT, DOUBLE, new double{}));
	CTXMAP(update).insert(ITEM(NMF_K_U_DT, DOUBLE, new double{}));
	CTXMAP(update).insert(ITEM(NMF_K_IU_FIRST_CELL, INT32, new int32_t{}));
	CTXMAP(update).insert(ITEM(NMF_K_U_ITER_AVG_TIME, DOUBLE, new double{}));
	CTXMAP(update).insert(ITEM(NMF_K_IU_LAST_CELL, INT32, new int32_t{}));
	CTXMAP(update).insert(ITEM(NMF_K_IU_COND_PROFILE, PROFILES, nullptr)); /* Must be initialized later */
	CTXMAP(update).insert(ITEM(NMF_K_IU_PH_PROFILE, PROFILES, nullptr)); /* Must be initialized later */
	CTXMAP(update).insert(ITEM(NMF_K_IU_PROFILES, PROFILES, nullptr)); /* Must be initialized later */
	CTXMAP(update).insert(ITEM(NMF_K_U_TIME, DOUBLE, new double{}));
	CTXMAP(update).insert(ITEM(NMF_K_IU_VOLTAGE, DOUBLE, new double{}));
	CTXMAP(update).insert(ITEM(NMF_K_IU_CFTYPE, CF_TYPE, new ConstantForce{}));
	CTXMAP(update).insert(ITEM(NMF_K_IU_DX, DOUBLE, new double{}));
	CTXMAP(update).insert(ITEM(NMF_K_IU_NUM_CELLS, INT32, new int32_t{}));
	CTXMAP(update).insert(ITEM(NMF_K_IU_CONSTITUENT_NAMES, STRINGS, new StringsImpl{}));
	CTXMAP(update).insert(ITEM(NMF_K_IU_NUM_CONSTITUENTS, INT32, new int32_t{}));

	CTXMAP(term).insert(ITEM(NMF_K_T_REASON, SIM_FAIL, new SimulationFailure{}));
}

PacketImpl makeInitPacket(ReporterContext &repCtx, const SimulationContext &simCtx)
{
	fillInitMap(repCtx.initMap->m_map, simCtx);

	return PacketImpl{repCtx.initMap.get()};
}

PacketImpl makeUpdatePacket(ReporterContext &repCtx, const SimulationContext &simCtx)
{
	fillUpdateMap(repCtx.updateMap->m_map, simCtx);

	return PacketImpl{repCtx.updateMap.get()};
}

PacketImpl makeTermPacket(ReporterContext &repCtx, const SimulationFailure fail)
{
	auto &map = repCtx.termMap->m_map;

	MSET(NMF_K_T_REASON, fail);

	return PacketImpl{repCtx.termMap.get()};
}

void prepareReporterContext(ReporterContext &repCtx, const SimulationContext &simCtx)
{
	auto initProf = [](auto &where, const int32_t NCO, const int32_t NCells, const char *id) {
		auto data = new double[NCO * NCells];
		auto profs = new ProfilesImpl{NCO, NCells, data};
		where->m_map[id].data = profs;
	};

	initProf(repCtx.initMap, 1, simCtx.NCells, NMF_K_IU_COND_PROFILE);
	initProf(repCtx.initMap, 1, simCtx.NCells, NMF_K_IU_PH_PROFILE);
	initProf(repCtx.updateMap, 1, simCtx.NCells, NMF_K_IU_COND_PROFILE);
	initProf(repCtx.updateMap, 1, simCtx.NCells, NMF_K_IU_PH_PROFILE);
	initProf(repCtx.initMap, simCtx.NCO, simCtx.NCells, NMF_K_IU_PROFILES);
	initProf(repCtx.updateMap, simCtx.NCO, simCtx.NCells, NMF_K_IU_PROFILES);

	auto &names = getRef<StringsImpl>(repCtx.updateMap->m_map, NMF_K_IU_CONSTITUENT_NAMES);
	for (size_t idx{0}; idx < simCtx.chemSystem.constituents->size(); idx++) {
		const auto &ctuent = simCtx.chemSystem.constituents->at(idx);
		names.append(ctuent->name->c_str());
	}
}

} // namespace NMF
