#include "itemmap_impl.h"

#include <cassert>
#include <cstdlib>

namespace NMF {

const Item & ECHMET_CC ItemMapImpl::get(const char *key) const
{
	try {
		return m_map.at(std::string{key});
	} catch (const std::out_of_range &) {
		assert(false);
		std::abort();
	}
}

bool ECHMET_CC ItemMapImpl::has(const char *key) const
{
	return m_map.find(std::string{key}) != m_map.cend();
}

ItemMap::~ItemMap()
{}

} // namespace NMF
