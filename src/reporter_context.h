#ifndef REPORTER_CONTEXT_H
#define REPORTER_CONTEXT_H

#include "itemmap_impl.h"

#include <memory>

namespace NMF {

class ReporterContext {
public:
	std::unique_ptr<ItemMapImpl> initMap;
	std::unique_ptr<ItemMapImpl> startMap;
	std::unique_ptr<ItemMapImpl> pauseMap;
	std::unique_ptr<ItemMapImpl> termMap;
	std::unique_ptr<ItemMapImpl> updateMap;
};

} // namespace NMF

#endif // REPORTER_CONTEXT_H
