#ifndef CONTAINERS_IMPL_H
#define CONTAINERS_IMPL_H

#include <nmf.h>

namespace NMF {

class ProfilesImpl : public Profiles {
public:
	explicit ProfilesImpl(const int32_t numRows, const int32_t numCells,
			      double *const data);
	~ProfilesImpl() override;

	const double * ECHMET_CC data(const int32_t cell) const override;
	void ECHMET_CC destroy() override;
	int32_t ECHMET_CC numCells() const override;
	int32_t ECHMET_CC numRows() const override;

	size_t size() const
	{
		return m_numRows * m_numCells;
	}

	double ECHMET_CC operator()(const int32_t rowIdx, const int32_t segmentIdx) const override;

	double *const m_data;

private:
	const int32_t m_numCells;
	const int32_t m_numRows;
};

class StringsImpl : public Strings {
public:
	explicit StringsImpl(const int32_t size = 0);
	~StringsImpl() override;

	void append(const char *str);
	void append(const std::string &str);
	void ECHMET_CC destroy() override;
	const ECHMET::FixedString * ECHMET_CC elem(const size_t idx) const override;
	size_t ECHMET_CC size() const override;

private:
	std::vector<ECHMET::FixedString *> m_data;
};

} // namespace NMF

#endif // CONTAINERS_IMPL_H
