#include "event_impl.h"

namespace NMF {

EventImpl::EventImpl() :
	m_type{EventType::TERMINATED},
	m_packet{PacketImpl{}},
	m_cleared{true}
{}

EventImpl::EventImpl(EventType _type, PacketImpl &&_packet) :
	m_type{_type},
	m_packet{std::move(_packet)},
	m_cleared{false}
{}

EventImpl::EventImpl(EventImpl &&other) noexcept :
	m_type{other.m_type},
	m_packet{const_cast<PacketImpl&&>(std::move(other.m_packet))},
	m_cleared{other.m_cleared}
{}

EventImpl & EventImpl::operator=(EventImpl &&other) noexcept
{
	const_cast<EventType&>(m_type) = other.m_type;
	const_cast<PacketImpl&>(m_packet) = const_cast<PacketImpl&&>(std::move(other.m_packet));
	m_cleared = other.m_cleared;

	return *this;
}

Event::~Event()
{}

} // namespace NMF
