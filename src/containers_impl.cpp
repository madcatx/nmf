#include "containers_impl.h"

#include <limits>

#include <cassert>

namespace NMF {

ProfilesImpl::ProfilesImpl(const int32_t numRows, const int32_t numCells,
			   double *const data):
	m_data{data},
	m_numCells{numCells},
	m_numRows{numRows}
{}

ProfilesImpl::~ProfilesImpl()
{
	delete [] m_data;
}

const double * ProfilesImpl::data(const int32_t cell) const
{
	assert(cell < m_numCells);

	return &m_data[cell * m_numRows];
}

void ECHMET_CC ProfilesImpl::destroy()
{
	delete this;
}

int32_t ECHMET_CC ProfilesImpl::numCells() const
{
	return m_numCells;
}

int32_t ECHMET_CC ProfilesImpl::numRows() const
{
	return m_numRows;
}

double ECHMET_CC ProfilesImpl::operator()(const int32_t rowIdx, const int32_t cellIdx) const
{
	assert(m_numRows > 0);
	assert(m_numCells > 0);

	return m_data[cellIdx * m_numRows + rowIdx];
}

Profiles::~Profiles()
{}

StringsImpl::StringsImpl(const int32_t size)
{
	m_data.reserve(size);
}

StringsImpl::~StringsImpl()
{
	for (auto &str : m_data)
		str->destroy();
}

void StringsImpl::append(const char *str)
{
	auto eStr = ECHMET::createFixedString(str);
	m_data.emplace_back(eStr);
}

void StringsImpl::append(const std::string &str)
{
	auto eStr = ECHMET::createFixedString(str.c_str());
	m_data.emplace_back(eStr);
}

void ECHMET_CC StringsImpl::destroy()
{
	delete this;
}

const ECHMET::FixedString * ECHMET_CC StringsImpl::elem(const size_t idx) const
{
	assert(idx < m_data.size());

	return m_data[idx];
}

size_t StringsImpl::size() const
{
	return m_data.size();
}

Strings::~Strings()
{}


} // namespace NMF
