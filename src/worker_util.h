#ifndef WORKER_UTIL_H
#define WORKER_UTIL_H

#include "packet_impl.h"

namespace NMF {

class ReporterContext;
class SimulationContext;

void destroyReporterContext(ReporterContext &ctx);
RetCode fillProfileSlice(const ProfileType type, const ECHMET::FixedString *name, const double x, SimulationContext &ctx, ProfileSlice &slice);
void initReporterContext(ReporterContext &ctx);
PacketImpl makeInitPacket(ReporterContext &repCtx, const SimulationContext &simCtx);
PacketImpl makeUpdatePacket(ReporterContext &repCtx, const SimulationContext &simCtx);
PacketImpl makeTermPacket(ReporterContext &repCtx, const SimulationFailure fail);
void prepareReporterContext(ReporterContext &repCtx, const SimulationContext &simCtx);

} // namespace NMF

#endif // WORKER_UTIL_H
