#include "packet_impl.h"

#include <cassert>

namespace NMF {

PacketImpl::PacketImpl(ItemMapImpl *const itemMap) :
	m_itemMap{itemMap}
{}

PacketImpl::PacketImpl(PacketImpl &&other) noexcept :
	m_itemMap{other.m_itemMap}
{}

const ItemMap * ECHMET_CC PacketImpl::itemMap() const
{
	assert(m_itemMap != nullptr);

	return m_itemMap;
}

PacketImpl & PacketImpl::operator=(PacketImpl &&other) noexcept
{
	const_cast<ItemMapImpl*&>(m_itemMap) = other.m_itemMap;

	return *this;
}

Packet::~Packet()
{}

} // namespace NMF
