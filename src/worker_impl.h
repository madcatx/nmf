#ifndef WORKER_IMPL_H
#define WORKER_IMPL_H

#include <nmf.h>

#include "event_impl.h"
#include "worker_util.h"
#include "reporter_context.h"
#include "simulation/simulation_util.h"
#include "simulation/simulator.h"

#include <atomic>
#include <cassert>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <thread>

namespace NMF {

class PacketImpl;

class WorkerInternal : public Worker {
public:
	virtual ~WorkerInternal();

	virtual EventHandlerFunc handler() const noexcept = 0;
	virtual void * handlerContext() const noexcept = 0;
	virtual RetCode moveState(WorkerInternal *other) noexcept = 0;
};

template <Stepper S, InstructionSet ISet>
class WorkerImpl : public WorkerInternal {
public:
	WorkerImpl(EventHandlerFunc handler, void *context) :
		m_runReporter{true},
		m_runWorker{true},
		m_handler{handler},
		m_handlerContext{context},
		m_simCtxMoved{false}
	{
		initReporterContext(m_repCtx);

		m_currentStatePacket = new PacketImpl{};

		m_reporterThread = std::thread{&WorkerImpl::reporterLoop, this};
	}

	~WorkerImpl() override
	{
		m_runWorker = false;
		m_runReporter = false;
		m_waitSendEvent.notify_one();
		if (m_reporterThread.joinable())
			m_reporterThread.join();
		if (m_workerThread.joinable())
			m_workerThread.join();

		if (!m_simCtxMoved)
			destroySimulationContext(m_simCtx);
		destroyReporterContext(m_repCtx);

		delete m_currentStatePacket;
	}

	RetCode ECHMET_CC currentState(Packet *&packet) override
	{
		if (m_workerThread.joinable()) {
			if (!m_workerFinished)
				return RetCode::E_BUSY;
		}

		if (!m_simCtx.isValid)
			return RetCode::E_WORKER_NOT_INITIALIZED;

		*m_currentStatePacket = makeUpdatePacket(m_repCtx, m_simCtx);
		packet = m_currentStatePacket;

		return RetCode::OK;
	}

	void ECHMET_CC destroy() override
	{
		delete this;
	}

	EventHandlerFunc handler() const noexcept override
	{
		return m_handler;
	}

	void * handlerContext() const noexcept override
	{
		return m_handlerContext;
	}

	bool ECHMET_CC isRunning() const override
	{
		if (!m_workerThread.joinable())
			return false;
		return !m_workerFinished;
	}

	RetCode ECHMET_CC initialize(const Initialization &init, InitializationFailure &fail) override
	{
		if (m_workerThread.joinable()) {
			if (!m_workerFinished)
				return RetCode::E_BUSY;
		}

		const auto tRet = initSimulationContext(init, m_simCtx, fail);
		if (tRet != RetCode::OK)
			return tRet;

		prepareReporterContext(m_repCtx, m_simCtx);

		try {
			initSimulation(m_simCtx);
		} catch (const SimulationError &ex) {
			return RetCode::E_CANNOT_ZERO_STEP;
		}

		sendEvent(EventType::INITIALIZED, makeInitPacket(m_repCtx, m_simCtx));

		return RetCode::OK;
	}

	RetCode moveState(WorkerInternal *other) noexcept override
	{
		auto tOther = static_cast<WorkerImpl *>(other);
		m_simCtx = std::move(tOther->m_simCtx);
		tOther->m_simCtxMoved = true;

		prepareReporterContext(m_repCtx, m_simCtx);

		return RetCode::OK;
	}

	RetCode ECHMET_CC pause() override
	{
		if (!m_workerThread.joinable())
			return RetCode::NOT_RUNNING;

		m_runWorker = false;
		m_workerThread.join();

		return RetCode::OK;
	}

	RetCode ECHMET_CC profileSlice(const ProfileType type, const ECHMET::FixedString *name, const double x, ProfileSlice &slice) override
	{
		if (m_workerThread.joinable()) {
			if (!m_workerFinished)
				return RetCode::E_BUSY;
		}

		if (!m_simCtx.isValid)
			return RetCode::E_WORKER_NOT_INITIALIZED;

		return fillProfileSlice(type, name, x, m_simCtx, slice);
	}

	RetCode ECHMET_CC start(const Start &params, StartFailure &fail) override
	{
		if (m_workerThread.joinable()) {
			if (!m_workerFinished)
				return RetCode::E_BUSY;
			else
				m_workerThread.join();
		}

		if (!m_simCtx.isValid)
			return RetCode::E_WORKER_NOT_INITIALIZED;

		const auto tRet = startupSimulationContext(params, m_simCtx, fail);
		if (tRet != RetCode::OK)
			return tRet;

		m_runWorker = true;
		m_workerThread = std::thread{&WorkerImpl::workerLoop, this};

		return RetCode::OK;
	}

	Stepper ECHMET_CC stepper() const override
	{
		return S;
	}

private:

	void handleUnsentEvent()
	{
		m_unsentEvent = false;

		switch (m_unsentEventType) {
		case EventType::INITIALIZED:
			sendEvent(EventType::STARTED, makeUpdatePacket(m_repCtx, m_simCtx));
			break;
		case EventType::STARTED:
			trySendEvent(EventType::STARTED, PacketImpl{});
			break;
		case EventType::DATA_UPDATED:
			trySendEvent(EventType::STARTED, makeUpdatePacket(m_repCtx, m_simCtx));
			break;
		case EventType::PAUSED:
			sendEvent(EventType::STARTED, makeUpdatePacket(m_repCtx, m_simCtx));
			break;
		case EventType::TERMINATED:
			sendEvent(EventType::STARTED, PacketImpl{});
			break;
		}
	}

	void trySendEvent(const EventType type, PacketImpl &&packet)
	{
		if (m_eventLockMtx.try_lock()) {
			m_unsentEvent = false;

			m_pendingEvent = EventImpl{type, std::move(packet)};
			m_eventLockMtx.unlock();
			m_waitSendEvent.notify_one();
		} else {
			m_unsentEvent = true;
			m_unsentEventType = type;
		}
	}

	void reporterLoop()
	{
		std::unique_lock<std::mutex> lk{m_eventLockMtx};

		while (m_runReporter) {
			m_waitSendEvent.wait(lk);

			if (!m_pendingEvent.isCleared()) {
				m_handler(*this, m_pendingEvent, m_handlerContext);
				m_pendingEvent.clear();
			}
		}
	}

	void sendEvent(const EventType type, PacketImpl &&packet)
	{
		m_eventLockMtx.lock();

		m_unsentEvent = false;
		m_pendingEvent = EventImpl{type, std::move(packet)};

		m_eventLockMtx.unlock();
		m_waitSendEvent.notify_one();
	}

	void workerLoop()
	{
		Simulator<S, ISet> simulator{m_simCtx};

		sendEvent(EventType::STARTED, PacketImpl{});

		m_workerFinished = false;
		double tLastUpdate{0};
		size_t iters{0};
		auto tLast = std::chrono::steady_clock::now();
		while ((m_simCtx.t < m_simCtx.tStop) && m_runWorker) {
			try {
				simulator.step();
				iters++;

				if (m_simCtx.t - tLastUpdate >= m_simCtx.updateInterval) {
					const auto now = std::chrono::steady_clock::now();
					const auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(now - tLast).count();
					m_simCtx.avgTimePerIter = double(diff) / iters;

					trySendEvent(EventType::DATA_UPDATED, makeUpdatePacket(m_repCtx, m_simCtx));

					tLastUpdate = m_simCtx.t;
					tLast = now;
					iters = 0;
				} else if (m_unsentEvent) {
					handleUnsentEvent();
				}
			} catch (const SimulationError &ex) {
				m_runWorker = false;
				sendEvent(EventType::TERMINATED, makeTermPacket(m_repCtx, ex.error()));
				return;
			}
		}

		sendEvent(EventType::PAUSED, makeUpdatePacket(m_repCtx, m_simCtx));
		m_workerFinished = true;
	}

	std::atomic<bool> m_runReporter;
	std::atomic<bool> m_runWorker;
	std::atomic<bool> m_workerFinished;

	std::mutex m_eventLockMtx;
	std::condition_variable m_waitSendEvent;
	EventImpl m_pendingEvent;

	bool m_unsentEvent;
	EventType m_unsentEventType;

	ReporterContext m_repCtx;
	SimulationContext m_simCtx;

	PacketImpl *m_currentStatePacket;

	EventHandlerFunc m_handler;
	void * const m_handlerContext;

	std::thread m_workerThread;
	std::thread m_reporterThread;

	bool m_simCtxMoved;
};

} // namespace NMF

#endif // WORKER_IMPL_H
