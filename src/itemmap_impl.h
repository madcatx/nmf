#ifndef NMF_ITEMMAP_IMPL_H
#define NMF_ITEMMAP_IMPL_H

#include <nmf.h>

#include <map>
#include <string>

namespace NMF {

class ItemMapImpl : public ItemMap {
public:
	const Item & ECHMET_CC get(const char *key) const override;
	bool ECHMET_CC has(const char *key) const override;

	std::map<std::string, Item> m_map;
};

} // namespace NMF

#endif // NMF_ITEMMAP_IMPL_H
