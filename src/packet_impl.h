#ifndef PACKET_IMPL_H
#define PACKET_IMPL_H

#include "itemmap_impl.h"

namespace NMF {

class PacketImpl : public Packet {
public:
	explicit PacketImpl(ItemMapImpl *const itemMap = nullptr);
	PacketImpl(PacketImpl &&other) noexcept;

	const ItemMap * ECHMET_CC itemMap() const override;

	PacketImpl & operator=(PacketImpl &&other) noexcept;

	ItemMapImpl *const m_itemMap;
};

} // namespace NMF

#endif // PACKET_IMPL_H
