#ifndef EVENT_IMPL_H
#define EVENT_IMPL_H

#include <nmf.h>

#include "packet_impl.h"

namespace NMF {

class EventImpl : public Event {
public:
	explicit EventImpl();
	EventImpl(EventType _type, PacketImpl &&_packet);
	EventImpl(EventImpl &&other) noexcept;

	void clear()
	{
		m_cleared = true;
	}

	bool isCleared() const
	{
		return m_cleared;
	}

	EventType type() const override
	{
		return m_type;
	}

	const Packet & packet() const override
	{
		return m_packet;
	}

	EventImpl & operator=(EventImpl &&) noexcept;

	const EventType m_type;
	const PacketImpl m_packet;

private:
	bool m_cleared;
};

} // namespace NMF


#endif // EVENT_IMPL_H
