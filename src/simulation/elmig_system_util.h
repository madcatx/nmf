#ifndef ELMIG_SYSTEM_UTIL_H
#define ELMIG_SYSTEM_UTIL_H

#include "simulation_common.h"
#include "simulation_context.h"
#include "vectorizers.h"

#include <echmetphchconsts.h>
#include <cassert>
#include <cstring>
#include <functional>

#ifndef LIBNMF_OMP_DISABLED
	#include <omp.h>
#endif // LIBNMF_OMP_DISABLED

namespace NMF {

class CalcDiffusionCurrent {
public:
	static void call(const OMPSize idx, const OMPSize shift,
			 const double totalCharge,
			 const size_t icIdx, double *itm,
			 SimulationContext &ctx)
	{
		itm[idx] += ECHMET::PhChConsts::F * totalCharge * ctx.diffusionCoefficients[(idx + shift) * ctx.NIF + icIdx] *
			    (ctx.pools.ionicConcentrations[(idx + 1 + shift) * ctx.NIF + icIdx] - ctx.pools.ionicConcentrations[(idx - 1 + shift) * ctx.NIF + icIdx]) * ctx.invH;
	}
};

/* TODO: Can we vectorize this ? */
class PrepFieldIntensity {
public:
	static void call(const OMPSize idx, const OMPSize shift, const double current, double *E)
	{
		E[idx + shift] = current;
	}
};

template <typename DataType>
class ElectricFieldSingleShotFunc {
public:
	static void call(const OMPSize idx, SimulationContext &ctx)
	{
		const auto ionicConcsLeft = &ctx.pools.ionicConcentrations[(idx - 1) * ctx.NIF];
		const auto ionicConcsRight = &ctx.pools.ionicConcentrations[(idx + 1) * ctx.NIF];
		const double *dcBlock = &ctx.diffusionCoefficients[idx * ctx.NIF];
		const DataType current = ctx.current;

		DataType jDiff{0.0};

		for (OMPSize jdx = 0; jdx < ctx.NIF; jdx++) {
			const size_t icIdx = ctx.ionicFormsInfo[jdx].concIdx;
			const double totalCharge = ctx.ionicFormsInfo[jdx].totalCharge;

			jDiff += ECHMET::PhChConsts::F * totalCharge * dcBlock[icIdx] * (ionicConcsRight[icIdx] - ionicConcsLeft[icIdx]) * ctx.invH;
		}

		ctx.electricFieldRawArray[idx] = (current + jDiff * ctx.areaRawArray[idx]) / (ctx.conductivityRawArray[idx] * ctx.areaRawArray[idx]);
	}
};

template <typename DataType, InstructionSet ISet>
class ElectricFieldLoopFunc {
public:
	static void call(const OMPSize from, const OMPSize to, SimulationContext &ctx)
	{
		typedef typename VectorizedLoopBlock<ISet>::type VType;
		typedef VFunctions<ISet, DataType> VF;
		typedef typename VF::VD VD;

		const DataType current = ctx.current;

		#pragma omp parallel for schedule(static)
		for (OMPSize idx = from; idx < to; idx += Alignment<ISet>::value) {
			VD intermediates;
			VF::zeroize(intermediates);

			for (OMPSize jdx = 0; jdx < ctx.NIF; jdx++) {
				const size_t icIdx = ctx.ionicFormsInfo[jdx].concIdx;
				const double totalCharge = ctx.ionicFormsInfo[jdx].totalCharge;

				Unroller<Alignment<ISet>::value, CalcDiffusionCurrent,
					 const size_t, const double, const size_t, DataType *, SimulationContext &>::unroll(idx, totalCharge, icIdx, intermediates, ctx);

			}

			Unroller<Alignment<ISet>::value, PrepFieldIntensity, const size_t, const DataType, DataType *>::unroll(idx, current, ctx.electricFieldRawArray);

			VType _E = VF::load(ctx.electricFieldRawArray + idx);
			/* jDiff * S */
			VType _a = VF::load(intermediates);		/* Diffusion current density */
			VType _b = VF::load(ctx.areaRawArray + idx);	/* Capillary area */
			_a = VF::mul(_a, _b);
			/* current + diffCurrent */
			_E = VF::add(_E, _a);
			/* totalCurrent / conductivity */
			_a = VF::load(ctx.conductivityRawArray + idx);	/* Conductivity */
			_E = VF::div(_E, _a);
			/* intermediate / area */
			_E = VF::div(_E, _b);
			VF::store(ctx.electricFieldRawArray + idx, _E);
		}
	}
};

template <typename DataType>
class ZeroizeUntouchedSingleShotFunc {
public:
	static void call(size_t idx, state_type &dxdt)
	{
		dxdt.data()[idx] = 0.0;
	}
};

template <typename DataType, InstructionSet ISet>
class ZeroizeUntouchedLoopFunc {
public:
	static void call(const OMPSize from, const OMPSize to, state_type &dxdt)
	{
#ifndef LIBNMF_OMP_DISABLED
		typedef typename VectorizedLoopBlock<ISet>::type VType;
		typedef VFunctions<ISet, DataType> VF;
		DataType *raw = dxdt.data();

		const VType zero = VF::zero();
#ifndef _WIN64
		#pragma omp parallel for schedule(static)
#endif // _WIN64
		for (OMPSize idx = from; idx < to; idx += Alignment<ISet>::value)
			VF::store(raw + idx, zero);
#endif // LIBNMF_OMP_DISABLED
	}
};

/*
 * Calculate conductivity in each cell
 */
template <InstructionSet ISet>
static
double calculateConductivity(SimulationContext &ctx)
{
	double totalResistance{0.0};	/* Total resistance of the entire capillary */

	#pragma omp parallel
	{
		double resistance{0.0};

	#ifndef LIBNMF_OMP_DISABLED
		const int tid{omp_get_thread_num()};
	#else
		const int tid{0};
	#endif // LIBNMF_OMP_DISABLED
		#pragma omp for schedule(static)
		for (OMPSize cell = 0; cell < ctx.NCells; cell++) {
			const double * ECHMET_RESTRICT_PTR ionMobsRaw = &ctx.pools.ionicMobilities[cell * ctx.NIF];
			const double * ECHMET_RESTRICT_PTR ionConcsRaw = &ctx.pools.ionicConcentrations[cell * ctx.NIF];

			double specCond{0.0};
			for (OMPSize idx = 0; idx < ctx.NIF; idx++) {
				specCond += ctx.ionicFormsAbsTotalCharge[idx] * ionMobsRaw[idx] * ionConcsRaw[idx];
					//specCond += totalCharge * ionMobsRaw[NIF] * ionConcsRaw[NIF] * PhChConsts::F * 1.0e-9;
					/* ^^ Mobility in 1e9 scale, c in mM/dm3 == mol/m3 !!! */
			}
			specCond *= ECHMET::PhChConsts::F * 1.0e-9;
			ctx.conductivityRawArray[cell] = specCond;

			resistance += ctx.dx / (ctx.areaRawArray[cell] * specCond);
		}

		ctx.blockResistance[tid] = resistance;
	}

	#ifndef LIBNMF_OMP_DISABLED
		for (auto idx{0}; idx < ctx.NThreads; idx++)
			totalResistance += ctx.blockResistance[idx];
	#else
		totalResistance = ctx.blockResistance[0];
	#endif // LIBNMF_OMP_DISABLED

	return totalResistance;
}

static
void calculateDiffusionCoefficients(const SimulationContext &ctx, const ECHMET::SysComp::CalculatedProperties &calcProps,
				    double *diffusionCoefficients)
{
	const auto &chemSystem = ctx.chemSystem;
	const double diffusionModifierCoeff = ctx.diffusionModifierCoeff;

	static const auto calcDiffCoeff = [](const double mobility, const int32_t charge) {
		const int32_t _charge = charge == 0 ? 1 : charge;

		return mobility * 1.0e-9 * ECHMET::PhChConsts::Tlab * ECHMET::PhChConsts::bk / (std::abs(_charge) * ECHMET::PhChConsts::e);
	};

	/* A note to your future self - DO NOT try to figure this out. */
	for (size_t idx = 0; idx < chemSystem.constituents->size(); idx++) {
		const ECHMET::SysComp::Constituent *c = chemSystem.constituents->elem(idx);

		for (size_t ifIdx = 0; ifIdx < c->ionicForms->size(); ifIdx++) {
			const ECHMET::SysComp::IonicForm *iF = c->ionicForms->elem(ifIdx);

			/* TODO: Skip ionic forms that were already processed */

			const ECHMET::ECHMETReal mobility = [&calcProps](const ECHMET::SysComp::IonicForm *iF, const ECHMET::SysComp::Constituent *c) -> double {
				if (iF->totalCharge != 0)
					return calcProps.ionicMobilities->elem(iF->ionicMobilityIndex);

				/* Our ionic form has zero charge. This does not work with our diffusion coefficient formula
				 * as it only works for charged particles. Approximate the diffusion coefficient from the
				 * charged ionic forms that are the most similar to our given form.
				 */
				const ECHMET::SysComp::IonicForm *chMinusOne = nullptr;
				const ECHMET::SysComp::IonicForm *chPlusOne = nullptr;
				auto findNextToZeroIFs = [](const ECHMET::SysComp::IonicForm *&chMinusOne, const ECHMET::SysComp::IonicForm *&chPlusOne, const ECHMET::SysComp::IonicForm *iF,  const ECHMET::SysComp::Constituent *c) {
					std::function<bool (const ECHMET::SysComp::IonicForm *, const ECHMET::SysComp::IonicForm *)> haveSameBuildingBlocks = [&haveSameBuildingBlocks](const ECHMET::SysComp::IonicForm *target, const ECHMET::SysComp::IonicForm *candidate) {
						/* Check whether both forms either are or are not a complex */
						const bool targetHasLigand = target->ligand != nullptr;
						const bool candidateHasLigand = candidate->ligand != nullptr;
						if (targetHasLigand != candidateHasLigand)
							return false;

						/* If we have complexes check if we have a same ligand */
						if (targetHasLigand && candidateHasLigand) {
							if (*(target->ligand->name) != *(candidate->ligand->name))
								return false;

							/* Check that both forms have the same ancestor structure. This is a fun one */
							const bool targetHasAncestor = target->ancestor != nullptr;
							const bool candidateHasAncestor = candidate->ancestor != nullptr;

							if (targetHasAncestor != candidateHasAncestor)
								return false;

							/* We have reached the bottom of the ancestor tree */
							if ((targetHasAncestor == false) && (candidateHasAncestor == false))
								return true;

							if (*(target->ancestor->nucleus->name) != *(candidate->ancestor->nucleus->name))
								return false;

							/* Dive down the ancestor tree with some recursive madness */
							return haveSameBuildingBlocks(target->ancestor, candidate->ancestor);
						}
						return *(target->nucleus->name) == *(candidate->nucleus->name);
					};

					for (size_t idx = 0; idx < c->ionicForms->size(); idx++) {
						const ECHMET::SysComp::IonicForm *ionicForm = c->ionicForms->elem(idx);

						/* TODO: return early once we find all the ionic forms we need */

						if (ionicForm->totalCharge == -1 && chMinusOne == nullptr) {
							if (haveSameBuildingBlocks(iF, ionicForm))
								chMinusOne = ionicForm;
						} else if (ionicForm->totalCharge == 1 && chPlusOne == nullptr) {
							if (haveSameBuildingBlocks(iF, ionicForm))
								chPlusOne = ionicForm;
						}
					}
				};

				findNextToZeroIFs(chMinusOne, chPlusOne, iF, c);

				/* Acid */
				if (chMinusOne && !chPlusOne)
					return calcProps.ionicMobilities->elem(chMinusOne->ionicMobilityIndex);
				/* Base */
				else if (!chMinusOne && chPlusOne)
					return calcProps.ionicMobilities->elem(chPlusOne->ionicMobilityIndex);
				/* Ampholyte */
				else if (chMinusOne && chPlusOne) {
					const double mobMinusOne = calcProps.ionicMobilities->elem(chMinusOne->ionicMobilityIndex);
					const double mobPlusOne = calcProps.ionicMobilities->elem(chPlusOne->ionicMobilityIndex);

					return (mobMinusOne + mobPlusOne) / 2.0;
				} else
					return 20.0; /* Arbitrarily chosen mobility for constituents that really have no charge
							Taken from Simul 5 to match the models better. */
			}(iF, c);

			const double diffCoeff = calcDiffCoeff(mobility, iF->totalCharge) * diffusionModifierCoeff;
			diffusionCoefficients[iF->ionicConcentrationIndex] = diffCoeff;
		}
	}

	/* H+ */
	diffusionCoefficients[0] = calcDiffCoeff(calcProps.ionicMobilities->elem(0), 1) * diffusionModifierCoeff;
	/* OH- */
	diffusionCoefficients[1] = calcDiffCoeff(calcProps.ionicMobilities->elem(1), 1) * diffusionModifierCoeff;
}

} // namespace NMF

#endif // ELMIG_SYSTEM_UTIL_H
