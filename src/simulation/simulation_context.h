#ifndef SIMULATION_CONTEXT_H
#define SIMULATION_CONTEXT_H

#include "simulation_common.h"

#include <nmf.h>

#include <memory>

namespace NMF {

class CalcSpaceAdaptor;

class SimulationContext {
public:
	SimulationContext() :
		isValid{false},
		conductivityRawArray{nullptr},
		electricFieldRawArray{nullptr},
		areaRawArray{nullptr},
		blockResistance{nullptr},
		solverCtx{nullptr},
		constituentsRaw{nullptr},
		calcSpaceAdaptor{nullptr}
	{}

	SimulationContext(const SimulationContext &) = delete;
	SimulationContext(SimulationContext &&) noexcept;
	SimulationContext & operator=(const SimulationContext &) = delete;
	SimulationContext & operator=(SimulationContext &&) noexcept;

	bool isValid;

	double updateInterval;

	double tStop;

	double t;
	double dt;

	state_type totalConcentrations;				/* <- This vector has [Constituents]*[Cells] dimension */
	state_type totalConcentrationsPrev;			/* <- This vector has [Constituents]*[Cells] dimension */
	state_type effectiveMobilities;				/* <- This vector has [Constituents]*[Cells] dimension */
	state_type diffusionCoefficients;			/* <- This vector has [IonicForms]*[Cells] dimension */
	CalculatedPropertiesVec calcPropsVec;			/* <- This vector has [Cells] dimension */
	ECHMET::SysComp::CalculatedPropertiesPools pools;

	double dx;
	double capillaryLength;

	double constantForceValue;
	ConstantForce constantForce;

	double *ECHMET_RESTRICT_PTR conductivityRawArray;
	double *ECHMET_RESTRICT_PTR electricFieldRawArray;
	double *ECHMET_RESTRICT_PTR areaRawArray;
	double *ECHMET_RESTRICT_PTR blockResistance;

	OMPSize NCO;
	OMPSize NIF;
	OMPSize NCells;

	OMPSize firstCell;
	OMPSize lastCell;

	int wallsCompromisedCtr;

	double diffusionModifierCoeff;		/* <- Default value should be 1.0 */

	ECHMET::SysComp::ChemicalSystem chemSystem;
	ECHMET::CAES::SolverContext *solverCtx;

	EqSolVec eqSolPacks;
	IonCtxVec ionCtxPacks;

	ECHMET::SysComp::Constituent *const *constituentsRaw;
	std::vector<IFPack> ionicFormsInfo;
	std::vector<RDCtuentIFsPack> constituentIonicFormsRaw;
	std::vector<double> ionicFormsAbsTotalCharge;

	bool useFullSolver;
	/* It is advantageous to store the nonideality corrections both
	 * in ECHMETCoreLibs native format and as simple bools */
	ECHMET::NonidealityCorrections corrs;
	bool correctDH;
	bool correctOF;

	double current;
	double voltage;
	double H;
	double invH;
	double invHH;

	ElectroosmoticFlowMode eofMode;
	double baseEofValue;

	int NThreads;
	double tolerance;

	bool spaceAdaptationEnabled;
	CalcSpaceAdaptor *calcSpaceAdaptor;

	double avgTimePerIter;
};

} // namespace NMF

#endif // SIMULATION_CONTEXT_H
