#include "calcspace_adaptor.h"

#include "simulation_context.h"

namespace NMF {

static const size_t CALCSPACE_HISTORY_SIZE{50};

CalcSpaceAdaptor::CalcSpaceAdaptor(const double _settlingOffset) :
	calcSpaceHistoryCtr{0},
	calcSpaceNotSet{true},
	settlingOffset{_settlingOffset}
{
	calcSpaceHistoryFirst.resize(CALCSPACE_HISTORY_SIZE);
	calcSpaceHistoryLast.resize(CALCSPACE_HISTORY_SIZE);
}

void CalcSpaceAdaptor::operator()(SimulationContext &ctx)
{
	static int firstPointGuard = 15;
	static int lastPointGuard = 15;

	const double t = ctx.t;
	const double dt = ctx.dt;
	const auto NCells = ctx.NCells;

	auto averageAdvance = [this](const std::vector<std::tuple<int, double>> &v) {
		int deltaIdx = 0.0;
		double deltaT = 0.0;

		for (size_t idx = 0; idx < CALCSPACE_HISTORY_SIZE - 1; idx++) {
			const size_t bufIdx = (calcSpaceHistoryCtr - idx) % CALCSPACE_HISTORY_SIZE;
			const size_t bufIdxPrev = (calcSpaceHistoryCtr - idx - 1) % CALCSPACE_HISTORY_SIZE;

			const int diffIdx = std::get<0>(v.at(bufIdx)) - std::get<0>(v.at(bufIdxPrev));
			const double diffT = std::get<1>(v.at(bufIdx)) - std::get<1>(v.at(bufIdxPrev));

			deltaIdx += diffIdx;
			deltaT += diffT;
		}

		return deltaIdx / deltaT;
	};

	if (t < 0.05 + settlingOffset) /* Settling phase, do nothing */
		return;

	const auto firstCalcPointPair = findFirstCalcPoint(ctx);
	const auto lastCalcPointPair = findLastCalcPoint(ctx);
	const auto firstCalcPoint = std::get<0>(firstCalcPointPair);
	const auto lastCalcPoint = std::get<0>(lastCalcPointPair);

	calcSpaceHistoryFirst[calcSpaceHistoryCtr % CALCSPACE_HISTORY_SIZE] = std::make_tuple(firstCalcPoint, t);
	calcSpaceHistoryLast[calcSpaceHistoryCtr % CALCSPACE_HISTORY_SIZE] = std::make_tuple(lastCalcPoint, t);

	if (firstCalcPoint - ctx.firstCell < 4 && firstCalcPoint > 4) {
		if (std::get<1>(firstCalcPointPair) > 1.5 * ctx.tolerance)
			ctx.wallsCompromisedCtr++;

		firstPointGuard *= 2;
	}

	if (ctx.lastCell - lastCalcPoint < 4 && lastCalcPoint < NCells - 5) {
		if (std::get<1>(lastCalcPointPair) > 1.5 * ctx.tolerance)
			ctx.wallsCompromisedCtr++;

		lastPointGuard *= 2;
	}

	if (calcSpaceHistoryCtr > CALCSPACE_HISTORY_SIZE) {
		if (calcSpaceNotSet) {
			ctx.firstCell = firstCalcPoint;
			ctx.lastCell = lastCalcPoint;
			calcSpaceNotSet = false;
		}

		const double advFirst = averageAdvance(calcSpaceHistoryFirst);
		const double advLast = averageAdvance(calcSpaceHistoryLast);

		auto calcGrowth = [](const double advance, const double dt) {
			return std::floor(advance * 1.05 * dt + 0.5);
		};

		ctx.firstCell = [&]() {
			int growth = calcGrowth(advFirst, dt) - firstPointGuard;
			OMPSize newPos = firstCalcPoint + growth;
			if (std::is_signed<OMPSize>::value) {
				if (newPos < 0 || newPos >= NCells)
					return 0;
			} else {
				if (newPos >= NCells)
					return 0;
			}
				return newPos;
		}();

		ctx.lastCell = [&]() {
			int growth = calcGrowth(advLast, dt) + lastPointGuard;
			OMPSize newPos = lastCalcPoint + growth;
			if (ctx.firstCell >= newPos || newPos >= NCells)
				return NCells - 1;
			return newPos;
		}();
	}

	calcSpaceHistoryCtr++;
}

std::pair<OMPSize, double> CalcSpaceAdaptor::findFirstCalcPoint(SimulationContext &ctx)
{
	const auto NCells = ctx.NCells;
	const auto NCO = ctx.NCO;

	for (OMPSize cell = ctx.firstCell; cell < NCells - 1; cell++) {
		for (OMPSize row = 0; row < NCO; row++) {
			const OMPSize idx = NCO * cell + row;

			const double cur = std::abs(ctx.totalConcentrations[idx]);
			const double prev = std::abs(ctx.totalConcentrationsPrev[idx]);
			const double low = std::min(cur, prev);

			ctx.totalConcentrationsPrev[idx] = ctx.totalConcentrations[idx];

			if (low == 0.0)
				continue;

			const double change = std::abs(cur - prev) / low;
			if (change > ctx.tolerance)
				return { cell, change };
		}
	}

	return { 0, 0.0 };
}

std::pair<OMPSize, double> CalcSpaceAdaptor::findLastCalcPoint(SimulationContext &ctx)
{
	const auto NCells = ctx.NCells;
	const auto NCO = ctx.NCO;

	for (OMPSize cell = ctx.lastCell; cell > ctx.firstCell; cell--) {
		for (OMPSize row = 0; row < NCO; row++) {
			const OMPSize idx = NCO * cell + row;

			const double cur = std::abs(ctx.totalConcentrations[idx]);
			const double prev = std::abs(ctx.totalConcentrationsPrev[idx]);
			const double low = std::min(cur, prev);

			ctx.totalConcentrationsPrev[idx] = ctx.totalConcentrations[idx];

			if (low == 0.0)
				continue;

			const double change = std::abs(cur - prev) / low;
			if (change > ctx.tolerance)
				return { cell, change };
		}
	}

	return { NCells - 1, 0.0 };
}

void CalcSpaceAdaptor::setSettlingOffset(const double offset)
{
	settlingOffset = offset;
	calcSpaceNotSet = true;
	calcSpaceHistoryCtr = 0;
}

} // namespace NMF
