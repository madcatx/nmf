#ifndef ELMIG_SYSTEM_H
#define ELMIG_SYSTEM_H

#include "elmig_system_util.h"

#include <cassert>

namespace NMF {

static
void calculateIonicMobilities(const OMPSize cell, const double *block, SimulationContext &ctx)
{
	/* NOTE: ECHMETCoreLibs provide the same function but a library call is
	 * noticably more expensive */
	double *efBlock = &ctx.effectiveMobilities[cell * ctx.NCO];

	const auto ionConcs = &ctx.pools.ionicConcentrations[cell * ctx.NIF];
	const auto ionMobs = &ctx.pools.ionicMobilities[cell * ctx.NIF];
	for (OMPSize row = 0; row < ctx.NCO; row++) {
		if (block[row] == 0.0) {
			efBlock[row] = 0.0;
			continue;
		}

		const auto &ctiFsRaw = ctx.constituentIonicFormsRaw[row];

		double effMob{0.0};
		for (size_t jdx{0}; jdx < ctiFsRaw.ionForms.size(); jdx++) {
			const auto &iF = ctiFsRaw.ionForms[jdx];

			effMob += ionConcs[iF.concIdx] * ionMobs[iF.mobIdx] * iF.xfrMult * iF.sgnCharge;
		}
		efBlock[row] = effMob / block[row];
	}
}

static
void calculateIonicDistributionFull(const state_type &x, SimulationContext &ctx)
{
	bool failed{false};

	/* Solve current equilibrium */
	#pragma omp parallel for schedule(dynamic, 32)
	for (OMPSize cell = ctx.firstCell; cell <= ctx.lastCell; cell++) {
	#ifndef LIBNMF_OMP_DISABLED
		const int tid{omp_get_thread_num()};
	#else
		const int tid{0};
	#endif // LIBNMF_OMP_DISABLED
		const double *block = &x[cell * ctx.NCO];

		ECHMET::CAES::Solver *solver = std::get<0>(ctx.eqSolPacks[tid]);
		auto acVec = std::get<1>(ctx.eqSolPacks[tid]);
		double *acVecRaw = std::get<2>(ctx.eqSolPacks[tid]);

		for (OMPSize idx{0}; idx < ctx.NCO; idx++)
			acVecRaw[idx] = block[idx] > 1.0e-13 ? block[idx] : 1.0e-13;

		auto tRet = solver->solve(acVec, ctx.calcPropsVec[cell], 1000, nullptr);
		if (tRet != ECHMET::RetCode::OK) {
			tRet = solver->estimateDistributionSafe(acVec, ctx.calcPropsVec[cell]);
			if (tRet != ECHMET::RetCode::OK)
				failed = true;
			else {
				tRet = solver->solve(acVec, ctx.calcPropsVec[cell], 1000, nullptr);
				failed = tRet != ECHMET::RetCode::OK;
			}
		}

		if (!ctx.correctOF)
			calculateIonicMobilities(cell, block, ctx);
	}

	if (failed)
		throw SimulationError{SimulationFailure::CANNOT_SOLVE_EQUILIBRIUM};
}

static
void calculateIonicDistributionFast(const state_type &x, SimulationContext &ctx)
{
	const size_t ACVEC_SIZE{ctx.NCO * sizeof(double)};

	bool failed{false};

	/* Solve current equilibrium */
	#pragma omp parallel for schedule(dynamic, 32)
	for (OMPSize cell = ctx.firstCell; cell <= ctx.lastCell; cell++) {
	#ifndef LIBNMF_OMP_DISABLED
		const int tid{omp_get_thread_num()};
	#else
		const int tid{0};
	#endif // LIBNMF_OMP_DISABLED
		const double *block = &x[cell * ctx.NCO];

		assert(cell >= 0 && cell < ctx.NCells);

		auto solver = std::get<0>(ctx.eqSolPacks[tid]);
		auto acVec = std::get<1>(ctx.eqSolPacks[tid]);
		double *acVecRaw = std::get<2>(ctx.eqSolPacks[tid]);

		std::memcpy(acVecRaw, &block[0], ACVEC_SIZE);

		// The first parameter gets the first element of the ionicConcentrations block for the given cell - this contains cH value
		auto tRet = solver->estimateDistributionFast(ctx.pools.ionicConcentrations[cell * ctx.NIF], acVec, ctx.calcPropsVec[cell]);
		if (tRet != ECHMET::RetCode::OK) {
			tRet = solver->estimateDistributionSafe(acVec, ctx.calcPropsVec[cell]);
			failed = tRet != ECHMET::RetCode::OK;
		}

		if (!ctx.correctOF)
			calculateIonicMobilities(cell, block, ctx);
	}

	if (failed)
		throw SimulationError{SimulationFailure::CANNOT_SOLVE_EQUILIBRIUM};
}

template <InstructionSet ISet>
class ElmigSystem {
public:

	ElmigSystem(SimulationContext &ctx) :
		m_ctx{ctx}
	{}

	void operator()(const state_type &x, state_type &dxdt, const double t)
	{
		(void)t;

		const double H = m_ctx.H;
		const double invH = m_ctx.invH;
		const double invHH = m_ctx.invHH;

		if (m_ctx.useFullSolver)
			calculateIonicDistributionFull(x, m_ctx);
		else
			calculateIonicDistributionFast(x, m_ctx);

		if (m_ctx.correctOF) {
			bool failed{false};

			/* Recalculate ionic mobilities as they affect diffusion (Nernst-Einstein) */
			#pragma omp parallel for schedule(static)
			for (OMPSize cell = m_ctx.firstCell; cell <= m_ctx.lastCell; cell++) {
			#ifndef LIBNMF_OMP_DISABLED
				const int tid {omp_get_thread_num()};
			#else
				const int tid{0};
			#endif // LIBNMF_OMP_DISABLED
				if (ECHMET::IonProps::correctMobilities(m_ctx.ionCtxPacks[tid], m_ctx.corrs, nullptr, m_ctx.calcPropsVec[cell]) == ECHMET::RetCode::OK) {
					const double *block = &x[cell * m_ctx.NCO];
					calculateIonicMobilities(cell, block, m_ctx);
				} else
					failed = true;
			}
			if (failed)
				throw SimulationError{SimulationFailure::CANNOT_CORRECT_MOBILITIES};

			/* Recalculate diffusion coefficients */
			#pragma omp parallel for schedule(static)
			for (OMPSize cell = m_ctx.firstCell; cell <= m_ctx.lastCell; cell++)
				calculateDiffusionCoefficients(m_ctx, m_ctx.calcPropsVec[cell], &m_ctx.diffusionCoefficients[cell * m_ctx.NIF]);
		}

		/* Total resistance of the entire capillary */
		const double totalResistance = calculateConductivity<ISet>(m_ctx);

		if (m_ctx.constantForce == ConstantForce::VOLTAGE) {
			m_ctx.current = m_ctx.constantForceValue / totalResistance;
			m_ctx.voltage = m_ctx.constantForceValue;
		} else {
			m_ctx.current = m_ctx.constantForceValue;
			m_ctx.voltage = totalResistance * m_ctx.constantForceValue;
		}

		double vEOF;
		if (m_ctx.eofMode == ElectroosmoticFlowMode::MOBILITY)
			vEOF = m_ctx.voltage / m_ctx.capillaryLength * m_ctx.baseEofValue;
		else
			vEOF = m_ctx.baseEofValue;

		const size_t firstCellElField = (m_ctx.firstCell > 2) ? m_ctx.firstCell - 1 : 1;
		const size_t lastCellElField = (m_ctx.lastCell < m_ctx.NCells - 3) ? m_ctx.lastCell + 1 : m_ctx.NCells - 2;

		/* Calculate electric field intensity for each cell */
		VectorizedLoop<ISet, double, SimulationContext, ElectricFieldSingleShotFunc, ElectricFieldLoopFunc>::call(firstCellElField, lastCellElField + 1, m_ctx);

		/* Left electric field boundary */
		if (m_ctx.firstCell < 2) {
			const auto ionConcsZero = m_ctx.pools.ionicConcentrations;
			const auto ionConcsTwo = &m_ctx.pools.ionicConcentrations[m_ctx.NIF * 2];
			const double *dcBlock = &m_ctx.diffusionCoefficients[0];
			double jDiff = 0.0;

			for (OMPSize idx {0}; idx < m_ctx.NIF; idx++) {
				const auto icIdx = m_ctx.ionicFormsInfo[idx].concIdx;
				const auto totalCharge = m_ctx.ionicFormsInfo[idx].totalCharge;

				jDiff += ECHMET::PhChConsts::F * totalCharge * dcBlock[icIdx] * (ionConcsTwo[icIdx] - ionConcsZero[icIdx]) / H;
			}

			m_ctx.electricFieldRawArray[0] = (m_ctx.current + jDiff * m_ctx.areaRawArray[0]) / (m_ctx.conductivityRawArray[0] * m_ctx.areaRawArray[0]);
		}
		/* Right electric field boundary */
		if (m_ctx.lastCell > m_ctx.NCells - 3) {
			const auto ionConcsLast2 = &m_ctx.pools.ionicConcentrations[(m_ctx.NCells - 3) * m_ctx.NIF];
			const auto ionConcsLast = &m_ctx.pools.ionicConcentrations[(m_ctx.NCells - 1) * m_ctx.NIF];
			const double *dcBlock = &m_ctx.diffusionCoefficients[(m_ctx.NCells - 1) * m_ctx.NIF];
			double jDiff{0.0};

			for (OMPSize idx = 0; idx < m_ctx.NIF; idx++) {
				const auto icIdx = m_ctx.ionicFormsInfo[idx].concIdx;
				const auto totalCharge = m_ctx.ionicFormsInfo[idx].totalCharge;

				jDiff += ECHMET::PhChConsts::F * totalCharge * dcBlock[icIdx] * (ionConcsLast[icIdx] - ionConcsLast2[icIdx]) / H;
			}

			m_ctx.electricFieldRawArray[m_ctx.NCells - 1] = (m_ctx.current + jDiff * m_ctx.areaRawArray[m_ctx.NCells - 1]) / (m_ctx.conductivityRawArray[m_ctx.NCells - 1] * m_ctx.areaRawArray[m_ctx.NCells - 1]);
		}

		const OMPSize firstCellMove = (m_ctx.firstCell != 0) ? m_ctx.firstCell : 1;
		const OMPSize lastCellMove = (m_ctx.lastCell != m_ctx.NCells - 1) ? m_ctx.lastCell : m_ctx.NCells - 2;
		/* Move it !!!! */
		#pragma omp parallel for schedule(static)
		for (OMPSize cell = firstCellMove; cell <= lastCellMove; cell++) {
			const auto ionConcsLeft = &m_ctx.pools.ionicConcentrations[(cell - 1) * m_ctx.NIF];
			const auto ionConcsCenter = &m_ctx.pools.ionicConcentrations[cell * m_ctx.NIF];
			const auto ionConcsRight = &m_ctx.pools.ionicConcentrations[(cell + 1) * m_ctx.NIF];
			const double areaLeft = m_ctx.areaRawArray[cell - 1];
			const double areaCenter = m_ctx.areaRawArray[cell];
			const double areaRight = m_ctx.areaRawArray[cell + 1];

			const double *dcBlockLeft = &m_ctx.diffusionCoefficients[(cell - 1) * m_ctx.NIF];
			const double *dcBlockCenter = &m_ctx.diffusionCoefficients[cell * m_ctx.NIF];
			const double *dcBlockRight = &m_ctx.diffusionCoefficients[(cell + 1) * m_ctx.NIF];
			const double *xBlockLeft = &x[(cell - 1) * m_ctx.NCO];
			const double *xBlockRight = &x[(cell + 1) * m_ctx.NCO];
			const double *efBlockLeft = &m_ctx.effectiveMobilities[(cell - 1) * m_ctx.NCO];
			const double *efBlockRight = &m_ctx.effectiveMobilities[(cell + 1) * m_ctx.NCO];

			double *dxdtBlock = &dxdt[cell * m_ctx.NCO];
			for (OMPSize row{0}; row < m_ctx.NCO; row++) {
				const auto &ctiFsRaw = m_ctx.constituentIonicFormsRaw[row];

				dxdtBlock[row] = 0.0;
				for (size_t jdx = 0; jdx < ctiFsRaw.ionForms.size(); jdx++) {
					const size_t icIdx = ctiFsRaw.ionForms[jdx].concIdx;
					const double cLeft = ionConcsLeft[icIdx];
					const double cCenter = ionConcsCenter[icIdx];
					const double cRight = ionConcsRight[icIdx];
					const double dLeft = dcBlockLeft[icIdx];
					const double dCenter = dcBlockCenter[icIdx];
					const double dRight = dcBlockRight[icIdx];

					const double X = (cRight - cLeft) * invH;
					const double Y = ((dRight - dLeft) * invH) + (dCenter * (areaRight - areaLeft) / (areaCenter * H));

					dxdtBlock[row] += dcBlockCenter[icIdx] * (cRight - 2.0 * cCenter + cLeft) * invHH;
					dxdtBlock[row] += X * Y;
				}

				const double elmigLeft = efBlockLeft[row] * m_ctx.electricFieldRawArray[cell - 1] * areaLeft * xBlockLeft[row];
				const double elmigRight = efBlockRight[row] * m_ctx.electricFieldRawArray[cell + 1] * areaRight * xBlockRight[row];
				const double elmigTerm = (elmigRight - elmigLeft) / (H * areaCenter * 1.0e9);

				dxdtBlock[row] -= elmigTerm;

				const double vEOF_areaCorrected = vEOF * m_ctx.areaRawArray[0] / areaCenter; /* vEOF is corrected relatively to vEOF at the first cell */
				dxdtBlock[row] -= vEOF_areaCorrected * (xBlockRight[row] - xBlockLeft[row]) * invH;
			}
		}

		/* Fill out the untouched dxdt with zeros.
		 * This works because of Column major ordering of the state_type and will break if it changes */
		VectorizedLoop<ISet, state_type::value_type, state_type, ZeroizeUntouchedSingleShotFunc, ZeroizeUntouchedLoopFunc>::call(0, firstCellMove * m_ctx.NCO , dxdt); /* Ahead of calculation space */
		VectorizedLoop<ISet, state_type::value_type, state_type, ZeroizeUntouchedSingleShotFunc, ZeroizeUntouchedLoopFunc>::call((lastCellMove + 1) * m_ctx.NCO, m_ctx.NCells * m_ctx.NCO , dxdt); /* After calculation space */

		/* Boundary conditions */
		if (m_ctx.firstCell == 0) {
			const auto ionConcs = &m_ctx.pools.ionicConcentrations[1 * m_ctx.NIF];
			const auto ionMobs = &m_ctx.pools.ionicMobilities[1 * m_ctx.NIF];
			double *dxdtBlock = &dxdt[0];
			double *dxdtBlockRight = &dxdt[m_ctx.NCO];

			for (OMPSize idx = 0; idx < m_ctx.NCO; idx++) {
				const auto &ctiFsRaw = m_ctx.constituentIonicFormsRaw[idx];

				double effectiveMobility{0.0};
				for (size_t jdx = 0; jdx < ctiFsRaw.ionForms.size(); jdx++) {
					const auto &iF = ctiFsRaw.ionForms[jdx];

					effectiveMobility += ionConcs[iF.concIdx] * ionMobs[iF.mobIdx] * iF.sgnCharge;
				}
				if (effectiveMobility * m_ctx.current < 0.0)/* Does this make an actual physical sense??? */
					dxdtBlock[idx] = dxdtBlockRight[idx]; /* Constituent is leaving the capillary */
				else
					dxdtBlock[idx] = 0.0;
			}


		}
		if (m_ctx.lastCell == m_ctx.NCells - 1) {
			const auto ionConcs = &m_ctx.pools.ionicConcentrations[(m_ctx.NCells - 2) * m_ctx.NIF];
			const auto ionMobs = &m_ctx.pools.ionicMobilities[(m_ctx.NCells - 2) * m_ctx.NIF];
			double *dxdtBlock = &dxdt[(m_ctx.NCells - 1) * m_ctx.NCO];
			double *dxdtBlockLeft = &dxdt[(m_ctx.NCells - 2) * m_ctx.NCO];

			for (OMPSize idx{0}; idx < m_ctx.NCO; idx++) {
				const auto &ctiFsRaw = m_ctx.constituentIonicFormsRaw[idx];

				double effectiveMobility{0.0};
				for (size_t jdx{0}; jdx < ctiFsRaw.ionForms.size(); jdx++) {
					const auto &iF = ctiFsRaw.ionForms[jdx];

					effectiveMobility += ionConcs[iF.concIdx] * ionMobs[iF.mobIdx] * iF.sgnCharge;
				}

				if (effectiveMobility * m_ctx.current > 0.0)/* Does this make an actual physical sense??? */
					dxdtBlock[idx] = dxdtBlockLeft[idx]; /* Constituent is leaving the capillary */
				else
					dxdtBlock[idx] = 0.0;

			}
		}
	}

private:
	SimulationContext &m_ctx;
};

} // namespace NMF

#endif // ELMIG_SYSTEM_H
