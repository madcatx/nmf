#include "simulator.h"

#define NMF_SSE2_ENABLED
#define NMF_AVX_ENABLED
#define NMF_FMA3_ENABLED
#define NMF_AVX512_ENABLED
#include "elmig_system.h"

namespace NMF {

template <>
Simulator<Stepper::RKCK54, InstructionSet::AVX512>::Simulator(SimulationContext &ctx) :
	m_ctx{ctx},
	m_sys{new ElmigSystem<InstructionSet::AVX512>{m_ctx}},
	m_obs{SimulationObserver{*m_ctx.calcSpaceAdaptor}},
	m_stp{ndf::make_controlled(m_ctx.tolerance,
				   m_ctx.tolerance,
				   rkck54_stepper_type())}
{}

template <>
Simulator<Stepper::RKCK54, InstructionSet::AVX512>::~Simulator()
{
	delete m_sys;
}

template <>
void Simulator<Stepper::RKCK54, InstructionSet::AVX512>::step()
{
	int ctr{0};
	while (++ctr < 10) {
		if (m_ctx.t + m_ctx.dt > m_ctx.tStop)
			m_ctx.dt = m_ctx.tStop - m_ctx.t;

		const auto ret = m_stp.try_step(*m_sys,
						m_ctx.totalConcentrations,
						m_ctx.t,
						m_ctx.dt);
		if (ret == ndf::controlled_step_result::success)
			break;
	}
	if (ctr >= 10)
		throw SimulationError{SimulationFailure::CANNOT_ADJUST_DT};

	m_obs(m_ctx);
}

} // namespace NMF
