#ifndef CALCSPACE_ADAPTOR_H
#define CALCSPACE_ADAPTOR_H

#include "simulation_common.h"

namespace NMF {

class SimulationContext;

class CalcSpaceAdaptor {
public:
	explicit CalcSpaceAdaptor(const double _settlingOffset);
	CalcSpaceAdaptor(const CalcSpaceAdaptor &) = delete;
	CalcSpaceAdaptor & operator=(const CalcSpaceAdaptor &) = delete;

	void operator()(SimulationContext &ctx);

	void setSettlingOffset(const double offset);

private:
	std::pair<OMPSize, double> findFirstCalcPoint(SimulationContext &ctx);
	std::pair<OMPSize, double> findLastCalcPoint(SimulationContext &ctx);

	size_t calcSpaceHistoryCtr;
	std::vector<std::tuple<int, double>> calcSpaceHistoryFirst;
	std::vector<std::tuple<int, double>> calcSpaceHistoryLast;
	bool calcSpaceNotSet;
	double settlingOffset;
};

} // namespace NMF

#endif // CALCSPACE_ADAPTOR_H
