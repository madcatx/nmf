#ifndef SIMULATION_UTIL_H
#define SIMULATION_UTIL_H

#include "simulation_context.h"

#include <nmf.h>

namespace NMF {

template <typename T>
inline
int sgn(const T &val)
{
	return (T(0) < val) - (val < T(0));
}

void destroySimulationContext(SimulationContext &ctx);
RetCode initSimulationContext(const Initialization &init, SimulationContext &ctx, InitializationFailure &fail);
void initSimulation(SimulationContext &ctx);
RetCode startupSimulationContext(const Start &init, SimulationContext &ctx, StartFailure &fail);

} // namespace NMF

#endif // SIMULATION_UTIL_H
