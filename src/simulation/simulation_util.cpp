#include "simulation_util.h"
#include "../containers_impl.h"

#include <echmetsyscomp.h>
#include <cassert>
#include <cmath>
#include <cstring>

#include "elmig_system_util.h"
#include "calcspace_adaptor.h"

#ifndef LIBNMF_OMP_DISABLED
#include <omp.h>
#endif // LIBNMF_OMP_DISABLED

namespace NMF {

static
void mm_zeroFree(double * ECHMET_RESTRICT_PTR &ptr)
{
	_mm_free(ptr);
	ptr = nullptr;
}

static
double calcSigmoid(const double x, const double pos, const double width)
{
	/* Steepness parameter of the sigmoid function */
	const double expE = 2.0 * std::log(0.000003) / width;

	if (x < (pos - 1.5 * width)) {
		return 0.0;
	} else if (x > (pos + 1.5 * width)) {
		return 1.0;
	} else {
		const double E = x - pos;
		const double vs = 1.0 / (1.0 + std::exp(expE * E));

		return vs;
	}
}

static
void applyCapillaryProfile(const std::vector<double> &segments, const std::vector<double> &diameters,
			   const std::vector<double> &edgesWidths,
			   SimulationContext &ctx)
{
	assert(diameters.size() > 0);
	assert(segments.size() == diameters.size() - 1);
	assert(segments.size() == edgesWidths.size());

	for (OMPSize col{0}; col < ctx.NCells; col++)
		ctx.areaRawArray[col] = diameters.at(0);

	/* Calculate diameters first */
	for (size_t seg{0}; seg < segments.size(); seg++) {
		const double edgePosition = segments.at(seg) * ctx.capillaryLength;
		const double dDelta = diameters.at(seg + 1) - diameters.at(seg);

		double x{0.0};
		for (OMPSize col{0}; col < ctx.NCells; col++) {
			ctx.areaRawArray[col] += dDelta * calcSigmoid(x, edgePosition, edgesWidths.at(seg));
			x += ctx.dx;
		}
	}

	/* Convert diameters to areas */
	for (OMPSize col{0}; col < ctx.NCells; col++)
		ctx.areaRawArray[col] = M_PI * ctx.areaRawArray[col] * ctx.areaRawArray[col] / 4.0;
}

static
void destroyCalcPropsVec(SimulationContext &ctx)
{
	for (auto &item : ctx.calcPropsVec)
		ECHMET::SysComp::releaseCalculatedPropertiesWithPools(item);

	ctx.calcPropsVec.clear();
	ECHMET::SysComp::releaseCalculatedPropertiesPools(ctx.pools);
}

static
void destroyEqSolPacks(SimulationContext &ctx)
{
	for (auto &item : ctx.eqSolPacks) {
		auto solver = std::get<0>(item);
		auto vec = std::get<1>(item);

		solver->destroy();
		vec->destroy();
	}

	ctx.eqSolPacks.clear();
}

static
void destroyIonCtxPack(SimulationContext &ctx)
{
	for (auto &item : ctx.ionCtxPacks)
		item->destroy();

	ctx.ionCtxPacks.clear();
}

static
int numThreads()
{
#ifndef LIBNMF_OMP_DISABLED
	int thr{};
	#pragma omp parallel
	{
		thr = omp_get_num_threads();
	}
	return thr;
#else
	return 1;
#endif // LIBNMF_OMP_DISABLED
}

void destroySimulationContext(SimulationContext &ctx)
{
	if (!ctx.isValid)
		return;

	mm_zeroFree(ctx.blockResistance);
	mm_zeroFree(ctx.conductivityRawArray);
	mm_zeroFree(ctx.electricFieldRawArray);
	mm_zeroFree(ctx.areaRawArray);

	if (ctx.solverCtx != nullptr) {
		ctx.solverCtx->destroy();
		ctx.solverCtx = nullptr;
	}

	destroyCalcPropsVec(ctx);
	destroyEqSolPacks(ctx);
	destroyIonCtxPack(ctx);
	ECHMET::SysComp::releaseChemicalSystem(ctx.chemSystem);

	ctx.ionicFormsInfo.clear();
	ctx.constituentIonicFormsRaw.clear();
	ctx.ionicFormsAbsTotalCharge.clear();
	ctx.wallsCompromisedCtr = 0;

	delete ctx.calcSpaceAdaptor;

	ctx.isValid = false;
}

RetCode initSimulationContext(const Initialization &init, SimulationContext &ctx, InitializationFailure &fail)
{
	destroySimulationContext(ctx);

	/* Basic sanity checks */
	if (init.capillaryLength <= 0.0) {
		fail = InitializationFailure::CAPILLARY_LENGTH;
		return RetCode::E_INVALID_ARGUMENT;
	}

	auto profilesImpl = dynamic_cast<ProfilesImpl *>(init.profiles);
	if (profilesImpl == nullptr) {
		fail = InitializationFailure::PROFILES;
		return RetCode::E_INVALID_ARGUMENT;
	}

	if (!init.adaptCalcSpace) {
		if (init.firstCell < 0 || init.lastCell >= init.cells || init.firstCell >= init.lastCell) {
			fail = InitializationFailure::FIRST_LAST_CELL;
			return RetCode::E_INVALID_ARGUMENT;
		}
	}

	RetCode errRet{};

	ECHMET::SysComp::CalculatedProperties calcProps{};

	auto tRet = ECHMET::SysComp::makeComposition(ctx.chemSystem, calcProps, init.composition);
	if (tRet != ECHMET::RetCode::OK) {
		switch (tRet) {
		case ECHMET::RetCode::E_INVALID_CONSTITUENT:
			fail = InitializationFailure::INVALID_CONSTITUENT;
			break;
		case ECHMET::RetCode::E_DATA_TOO_LARGE:
			fail = InitializationFailure::SYSTEM_TOO_LARGE;
			break;
		case ECHMET::RetCode::E_INVALID_COMPLEXATION:
			fail = InitializationFailure::INVALID_COMPLEXATION;
			break;
		case ECHMET::RetCode::E_DUPLICIT_CONSTITUENTS:
			fail = InitializationFailure::DUPLICIT_CONSTITUENT;
			break;
		case ECHMET::RetCode::E_NO_MEMORY:
			return RetCode::E_NO_MEMORY;
		default:
			fail = InitializationFailure::COMPOSITION_UNSPECIFIED;
			break;
		}

		return RetCode::E_INVALID_ARGUMENT;
	}

	if (ctx.chemSystem.constituents->size() >= size_t(std::numeric_limits<OMPSize>::max())) {
		errRet = RetCode::E_INVALID_ARGUMENT;
		fail = InitializationFailure::SYSTEM_TOO_LARGE;
		goto fail_composition;
	}
	if (ctx.chemSystem.ionicForms->size() >= size_t(std::numeric_limits<OMPSize>::max())) {
		errRet = RetCode::E_INVALID_ARGUMENT;
		fail = InitializationFailure::SYSTEM_TOO_LARGE;
		goto fail_composition;
	}

	tRet = ECHMET::SysComp::allocateCalculatedPropertiesPools(ctx.pools, ctx.chemSystem, init.cells);
	if (tRet != ECHMET::RetCode::OK) {
		errRet = RetCode::E_NO_MEMORY;
		fail = InitializationFailure::UNKNOWN;
		goto fail_composition;
	}

	ctx.NCO = static_cast<OMPSize>(ctx.chemSystem.constituents->size());
	ctx.NIF = static_cast<OMPSize>(ctx.chemSystem.ionicForms->size());
	ctx.NCells = init.cells;
	ctx.NThreads = numThreads();
	ctx.t = 0.0;
	ctx.tStop = 0.0;
	ctx.dt = -1.0;
	ctx.updateInterval = -1.0;
	ctx.tolerance = -1.0;

	ctx.constantForceValue = init.constantForceValue;
	ctx.constantForce = init.constantForce;

	if (ctx.constantForce == ConstantForce::VOLTAGE) {
		ctx.voltage = ctx.constantForceValue;
		ctx.current = 0.0;
	} else {
		ctx.voltage = 0.0;
		ctx.current = ctx.constantForceValue;
	}

	ctx.capillaryLength = init.capillaryLength;
	ctx.dx = ctx.capillaryLength / ctx.NCells;
	ctx.H = 2.0 * ctx.dx;
	ctx.invH = 1.0 / ctx.H;
	ctx.invHH = 1.0 / (ctx.dx * ctx.dx);
	ctx.diffusionModifierCoeff = 1.0;  /* TODO: Make it adjustable in the future */
	ctx.spaceAdaptationEnabled = init.adaptCalcSpace;
	ctx.calcSpaceAdaptor = new CalcSpaceAdaptor{0.0};

	ctx.baseEofValue = 0.0;
	ctx.corrs = init.corrections;
	ctx.correctOF = ECHMET::nonidealityCorrectionIsSet(ctx.corrs, ECHMET::NonidealityCorrectionsItems::CORR_ONSAGER_FUOSS);
	ctx.correctDH = ECHMET::nonidealityCorrectionIsSet(ctx.corrs, ECHMET::NonidealityCorrectionsItems::CORR_DEBYE_HUCKEL);

	/* Fill raw data section
	 *
	 * We use a lot of precomputed raw data to offset
	 * any overhead of vector accesses through virtual methods
	 */
	ctx.constituentsRaw = ctx.chemSystem.constituents->cdata();

	for (size_t idx = 0; idx < ctx.chemSystem.ionicForms->size(); idx++) {
		const auto iF = ctx.chemSystem.ionicForms->elem(idx);
		ctx.ionicFormsInfo.push_back({iF->ionicConcentrationIndex, double(iF->totalCharge)});
	}

	for (OMPSize idx{0}; idx < ctx.NCO; idx++) {
		const auto ctuent = ctx.chemSystem.constituents->elem(idx);
		std::vector<double> xfrMults{};
		std::vector<double> sgnCharge{};

		for (size_t jdx{0}; jdx < ctuent->ionicForms->size(); jdx++) {
			const auto iF = ctuent->ionicForms->elem(jdx);
			int v;

			if (ECHMET::IonProps::getTransferMultiplier(ctuent, iF, v) != ECHMET::RetCode::OK) {
				errRet = RetCode::E_RUNTIME_ERROR;
				goto fail_ifs_raw;
			}

			xfrMults.emplace_back(v);
			sgnCharge.emplace_back(sgn(iF->totalCharge));
		}

		ctx.constituentIonicFormsRaw.emplace_back(ctuent->ionicForms->cdata(),
							  ctuent->ionicForms->size(),
							  std::move(xfrMults),
							  std::move(sgnCharge));

	}

	for (OMPSize idx{0}; idx < ctx.NIF; idx++)
		ctx.ionicFormsAbsTotalCharge.emplace_back(std::abs(ctx.chemSystem.ionicForms->elem(idx)->totalCharge));

	if (ctx.spaceAdaptationEnabled) {
		ctx.firstCell = 0;
		ctx.lastCell = ctx.NCells - 1;
	} else {
		ctx.firstCell = init.firstCell;
		ctx.lastCell = init.lastCell;
	}

	/* Check if we have to deal with any complexation */
	ctx.useFullSolver = false;
	for (OMPSize idx{0}; idx < ctx.NCO; idx++) {
		const auto ctuent = ctx.chemSystem.constituents->elem(idx);

		if (ctuent->ctype == ECHMET::SysComp::ConstituentType::NUCLEUS) {
			for (size_t ifIdx{0}; ifIdx < ctuent->ionicForms->size(); ifIdx++) {
				const auto iF = ctuent->ionicForms->at(ifIdx);
				if (iF->ligand != nullptr) {
					ctx.useFullSolver = true;
					break;
				}
			}
		}

		if (ctx.useFullSolver == true)
			break;
	}

	/* Prepare scratch arrays */
	ctx.totalConcentrations.resize(ctx.NCO * ctx.NCells);
	ctx.totalConcentrationsPrev.resize(ctx.NCO * ctx.NCells);
	ctx.effectiveMobilities.resize(ctx.NCO * ctx.NCells);
	ctx.diffusionCoefficients.resize(ctx.NIF * ctx.NCells);

	ctx.blockResistance = alignedAlloc<double>(ctx.NThreads);
	ctx.conductivityRawArray = alignedAlloc<double>(ctx.NCells);
	ctx.electricFieldRawArray = alignedAlloc<double>(ctx.NCells);
	ctx.areaRawArray = alignedAlloc<double>(ctx.NCells);

	ctx.calcPropsVec.resize(ctx.NCells);
	for (NMF::OMPSize cell{0}; cell < ctx.NCells; cell++) {
		auto &cp = ctx.calcPropsVec[cell];
		tRet = ECHMET::SysComp::initializeCalculatedPropertiesWithPools(cp, ctx.chemSystem, ctx.pools, cell);

		if (tRet != ECHMET::RetCode::OK) {
			errRet = RetCode::E_RUNTIME_ERROR;
			goto fail_calcprops;
		}
	}

	/* TODO: This must be handled in a different way if
	 * we intend to support variable capillary diameter properly!
	 */
	{
		std::vector<double> segments{}; /* Empty vector => only one segment */
		std::vector<double> diameters = { init.capillaryDiameter };
		std::vector<double> edgesWidths{}; /* Empty vector => only one segment */
		applyCapillaryProfile(segments, diameters, edgesWidths, ctx);
	}

	/* Prepare equilibrium solvers */
	tRet = ECHMET::CAES::createSolverContext(ctx.solverCtx, ctx.chemSystem);
	if (tRet != ECHMET::RetCode::OK) {
		errRet = RetCode::E_RUNTIME_ERROR;
		goto fail_calcprops;
	}
	for (int idx{0}; idx < ctx.NThreads; idx++) {
		using ECHMET::EnumOps::operator|;
		auto solver = ECHMET::CAES::createSolver(ctx.solverCtx, ECHMET::CAES::Solver::defaultOptions() | ECHMET::CAES::Solver::DISABLE_THREAD_SAFETY, ctx.corrs);

		if (solver == nullptr) {
			errRet = RetCode::E_RUNTIME_ERROR;
			goto fail_solvers;
		}

		auto v = ECHMET::createRealVec(ctx.NCO);
		if (v == nullptr) {
			solver->destroy();
			errRet = RetCode::E_RUNTIME_ERROR;
			goto fail_solvers;
		}
		v->resize(ctx.NCO);

		ctx.eqSolPacks.emplace_back(solver, v, v->data());

		auto ionCtx = ECHMET::IonProps::makeComputationContext(ctx.chemSystem, ECHMET::IonProps::ComputationContext::Options::DISABLE_THREAD_SAFETY);
		if (ionCtx == nullptr) {
			errRet = RetCode::E_RUNTIME_ERROR;
			goto fail_solvers;
		}

		ctx.ionCtxPacks.emplace_back(ionCtx);
	}

	ctx.avgTimePerIter = 0.0;

	/* Set up initial concentration profile */
	std::memcpy(ctx.totalConcentrations.data(), profilesImpl->m_data, profilesImpl->size() * sizeof(double));
	std::memcpy(ctx.totalConcentrationsPrev.data(), profilesImpl->m_data, profilesImpl->size() * sizeof(double));

	ECHMET::SysComp::releaseCalculatedProperties(calcProps); /* This is a dummy value anyway */

	ctx.isValid = true;

	return RetCode::OK;

fail_solvers:
	destroyIonCtxPack(ctx);
	destroyEqSolPacks(ctx);
fail_calcprops:
	destroyCalcPropsVec(ctx);
fail_ifs_raw:
	ctx.constituentIonicFormsRaw.clear();
fail_composition:
	ECHMET::SysComp::releaseChemicalSystem(ctx.chemSystem);
	ECHMET::SysComp::releaseCalculatedProperties(calcProps);

	return errRet;
}

void initSimulation(SimulationContext &ctx)
{
	bool failed{false};

	#pragma omp parallel for schedule(dynamic, 32)
	for (OMPSize cell = 0; cell < ctx.NCells; cell++) {
	#ifndef LIBNMF_OMP_DISABLED
		const int tid{omp_get_thread_num()};
	#else
		const int tid{0};
	#endif // LIBNMF_OMP_DISABLED
		const double *block = &ctx.totalConcentrations[ctx.NCO * cell];
		auto &calcProps = ctx.calcPropsVec[cell];
		ECHMET::CAES::SolverIterations iters;

		ECHMET::CAES::Solver *solver = std::get<0>(ctx.eqSolPacks[tid]);
		ECHMET::RealVec *acVec = std::get<1>(ctx.eqSolPacks[tid]);

		for (OMPSize idx = 0; idx < ctx.NCO; idx++)
			(*acVec)[idx] = block[idx];

		auto tRet = solver->estimateDistributionSafe(acVec, calcProps);
		if (tRet != ECHMET::RetCode::OK)
			failed = true;
		else {
			if (ctx.useFullSolver) {
				tRet = solver->solve(acVec, calcProps, 1000, &iters);
				failed = tRet != ECHMET::RetCode::OK;
			}
		}
	}

	if (failed)
		throw SimulationError{SimulationFailure::CANNOT_SOLVE_EQUILIBRIUM};

	if (ctx.correctOF) {
		bool failed{false};

		/* Recalculate ionic mobilities as they affect diffusion (Nernst-Einstein) */
		#pragma omp parallel for schedule(static)
		for (OMPSize cell = 0; cell < ctx.NCells; cell++) {
		#ifndef LIBNMF_OMP_DISABLED
			const int tid{omp_get_thread_num()};
		#else
			const int tid{0};
		#endif // LIBNMF_OMP_DISABLED
			auto &calcProps = ctx.calcPropsVec[cell];

			failed = ECHMET::IonProps::correctMobilities(ctx.ionCtxPacks[tid], ctx.corrs, nullptr, calcProps) != ECHMET::RetCode::OK;
		}

		if (failed)
			throw SimulationError{SimulationFailure::CANNOT_CORRECT_MOBILITIES};
	}

	/* Calculate diffusion coefficients */
	#pragma omp parallel for schedule(static)
	for (OMPSize cell = 0; cell < ctx.NCells; cell++)
		calculateDiffusionCoefficients(ctx, ctx.calcPropsVec.at(cell), &ctx.diffusionCoefficients[cell * ctx.NIF]);

	calculateConductivity<InstructionSet::SSE2>(ctx);
}

RetCode startupSimulationContext(const Start &params, SimulationContext &ctx, StartFailure &fail)
{
	/* Basic sanity checks */
	if (params.updateInterval <= 0.0) {
		fail = StartFailure::UPDATE_INTERVAL;
		return RetCode::E_INVALID_ARGUMENT;
	}

	if (params.dt <= 0.0) {
		fail = StartFailure::DT;
		return RetCode::E_INVALID_ARGUMENT;
	}

	if (params.tolerance <= 0) {
		fail = StartFailure::TOLERANCE;
		return RetCode::E_INVALID_ARGUMENT;
	}

	if (params.firstCell < 0 || params.lastCell >= ctx.NCells || params.firstCell >= params.lastCell) {
		fail = StartFailure::FIRST_LAST_CELL;
		return RetCode::E_INVALID_ARGUMENT;
	}

	if (params.tStop <= 0.0) {
		fail = StartFailure::T_STOP;
		return RetCode::E_INVALID_ARGUMENT;
	}
	if (params.tStop < ctx.tStop)
		return RetCode::DONE;
	ctx.tStop = params.tStop;

	if (!ctx.spaceAdaptationEnabled && params.adaptCalcSpace) {
		/* Space adaptation was disabled and now got enabled.
		 * Set walls to cover the entire space and shrink it later if possible */
		ctx.firstCell = 0;
		ctx.lastCell = ctx.NCells - 1;
		ctx.calcSpaceAdaptor->setSettlingOffset(ctx.t);
	} else if (ctx.spaceAdaptationEnabled && params.adaptCalcSpace) {
		/* Space adapation stays enabled, do not touch the position of first and last cell */
	} else {
		/* Disable space adaptation */
		ctx.firstCell = params.firstCell;
		ctx.lastCell = params.lastCell;
	}
	ctx.spaceAdaptationEnabled = params.adaptCalcSpace;

	ctx.dt = params.dt;
	ctx.updateInterval = params.updateInterval;
	ctx.tolerance = std::pow(10, -params.tolerance);

	ctx.constantForceValue = params.constantForceValue;
	ctx.constantForce = params.constantForce;

	if (ctx.constantForce == ConstantForce::VOLTAGE) {
		ctx.voltage = ctx.constantForceValue;
		ctx.current = 0.0;
	} else {
		ctx.voltage = 0.0;
		ctx.current = ctx.constantForceValue;
	}

	ctx.eofMode = params.eofMode;
	ctx.baseEofValue = params.eofValue;

	return RetCode::OK;
}

} // namespace NMF
