#ifndef XFISH_OMPVECTOR_H
#define XFISH_OMPVECTOR_H

#include <limits>
#include <stdexcept>
#include <type_traits>

#ifdef HAVE_CPP_17
#include <new>
#define OMPVEC_DEF_ALIGN std::hardware_constructive_interference_size;
#else
#define OMPVEC_DEF_ALIGN 64
#endif // OMPVEC_DEF_ALIGN

#ifdef ECHMET_COMPILER_MSVC
	#include <intrin.h>
#else
	#include <x86intrin.h>
#endif // ECHMET_COMPILER_MSVC

/*
 * This is a simple implementation of aligned data storage
 * with a subset of std::vector API. The main difference is
 * the variable index type instead of size_t. This is necessary
 * to make it usabe by some OpenMP implementations that cannot
 * parallelize for loops around unsigned indices.
 */

template <size_t>
class DifferenceType {};

template <>
class DifferenceType<4> {
public:
	using type = int32_t;
};

template <>
class DifferenceType<8> {
public:
	using type = int64_t;
};

template <typename T>
class OMPVectorIterator {
public:
	using iterator_category = std::random_access_iterator_tag;
	using value_type = T;
	using difference_type = DifferenceType<sizeof(void *)>::type;
	using pointer = T*;
	using reference = T&;

	OMPVectorIterator() :
		m_current{nullptr}
	{
	}

	explicit OMPVectorIterator(pointer m_ptr) :
		m_current{m_ptr}
	{
	}

	OMPVectorIterator(const OMPVectorIterator &other) :
		m_current{other.m_current}
	{
	}

	template <typename V = T, typename = typename std::enable_if<std::is_same<V, const typename std::remove_const<V>::type>::value, V>::type>
	OMPVectorIterator(const OMPVectorIterator<typename std::remove_const<V>::type> &other) :
		m_current{other.base()}
	{
	}

	template <typename V = T, typename = typename std::enable_if<std::is_same<V, const typename std::remove_const<V>::type>::value, V>::type>
	OMPVectorIterator operator=(const OMPVectorIterator<typename std::remove_const<V>::type> &other)
	{
		m_current = other.base();
	}

	pointer base() const noexcept
	{
		return m_current;
	}

	reference operator*() const noexcept
	{
		return *m_current;
	}

	pointer operator->() const noexcept
	{
		return m_current;
	}

	OMPVectorIterator & operator++() noexcept
	{
		++m_current;
		return *this;
	}

	OMPVectorIterator operator++(int) noexcept
	{
		return OMPVectorIterator{++m_current};
	}

	OMPVectorIterator & operator--() noexcept
	{
		--m_current;
		return *this;
	}

	OMPVectorIterator operator--(int) noexcept
	{
		return OMPVectorIterator(--m_current);
	}

	reference operator[](const difference_type offset) const noexcept
	{
		return m_current[offset];
	}

	OMPVectorIterator & operator+=(const difference_type step) noexcept
	{
		m_current += step;
		return *this;
	}

	OMPVectorIterator & operator-=(const difference_type step) noexcept
	{
		m_current -= step;
		return *this;
	}

	OMPVectorIterator operator+(const difference_type step) const noexcept
	{
		return OMPVectorIterator{m_current + step};
	}

	OMPVectorIterator operator-(const difference_type step) const noexcept
	{
		return OMPVectorIterator{m_current - step};
	}

private:
	pointer m_current;
};

/* Forward iterator requirements */
template <typename T>
inline
bool operator==(const OMPVectorIterator<T> &lhs, const OMPVectorIterator<T> &rhs) noexcept
{
	return lhs.base() == rhs.base();
}

template <typename TL, typename TR>
inline
bool operator==(const OMPVectorIterator<TL> &lhs, const OMPVectorIterator<TR> &rhs) noexcept
{
	return lhs.base() == rhs.base();
}

template <typename T>
inline
bool operator!=(const OMPVectorIterator<T> &lhs, const OMPVectorIterator<T> &rhs) noexcept
{
	return lhs.base() != rhs.base();
}

template <typename TL, typename TR>
inline
bool operator!=(const OMPVectorIterator<TL> &lhs, const OMPVectorIterator<TR> &rhs) noexcept
{
	return lhs.base() != rhs.base();
}

/* Random access iterator requires comparison operators */
template <typename T>
inline
bool operator<(const OMPVectorIterator<T> &lhs, const OMPVectorIterator<T> &rhs) noexcept
{
	return lhs.base() < rhs.base();
}

template <typename TL, typename TR>
inline
bool operator<(const OMPVectorIterator<TL> &lhs, const OMPVectorIterator<TR> &rhs) noexcept
{
	return lhs.base() < rhs.base();
}

template <typename T>
inline
bool operator<=(const OMPVectorIterator<T> &lhs, const OMPVectorIterator<T> &rhs) noexcept
{
	return lhs.base() <= rhs.base();
}

template <typename TL, typename TR>
inline
bool operator<=(const OMPVectorIterator<TL> &lhs, const OMPVectorIterator<TR> &rhs) noexcept
{
	return lhs.base() <= rhs.base();
}

template <typename T>
inline
bool operator>(const OMPVectorIterator<T> &lhs, const OMPVectorIterator<T> &rhs) noexcept
{
	return lhs.base() > rhs.base();
}

template <typename TL, typename TR>
inline
bool operator>(const OMPVectorIterator<TL> &lhs, const OMPVectorIterator<TR> &rhs) noexcept
{
	return lhs.base() > rhs.base();
}

template <typename T>
inline
bool operator>=(const OMPVectorIterator<T> &lhs, const OMPVectorIterator<T> &rhs) noexcept
{
	return lhs.base() >= rhs.base();
}


template <typename TL, typename TR>
inline
bool operator>=(const OMPVectorIterator<TL> &lhs, const OMPVectorIterator<TR> &rhs) noexcept
{
	return lhs.base() >= rhs.base();
}

template <typename T>
inline
typename OMPVectorIterator<T>::difference_type
operator-(const OMPVectorIterator<T> &lhs, const OMPVectorIterator<T> &rhs) noexcept
{
	return lhs.base() - rhs.base();
}

template <typename TL, typename TR>
inline
typename OMPVectorIterator<TL>::difference_type
operator-(const OMPVectorIterator<TL> &lhs, const OMPVectorIterator<TR> &rhs) noexcept
{
	return lhs.base() - rhs.base();
}

template <typename T>
inline
typename OMPVectorIterator<T>::difference_type
operator+(const OMPVectorIterator<T> &lhs, const OMPVectorIterator<T> &rhs) noexcept
{
	return lhs.base() + rhs.base();
}

template <typename T, typename S = int, std::size_t Alignment = OMPVEC_DEF_ALIGN>
class OMPVector {
public:
	using size_type = S;
	using value_type = T;
	using iterator = OMPVectorIterator<T>;
	using const_iterator = OMPVectorIterator<const T>;

	static_assert(Alignment % sizeof(T) == 0,
		      "Stored type cannot be aligned to the requested boundary");

	static_assert(std::numeric_limits<size_type>::max() <= std::numeric_limits<std::size_t>::max(),
		      "Maximum value of requested index type is greater than maximum value of internal index type");

	static_assert(size_t(std::numeric_limits<int>::max()) <= std::numeric_limits<std::size_t>::max(),
		      "Numeric range of std::size_t is too small");

	OMPVector()
	{
		m_data = getAligned(DEFAULT_ALLOC);
		m_allocated = DEFAULT_ALLOC;
		m_used = 0;
	}

	explicit OMPVector(const size_type N)
	{
		checkNegative(N);

		const auto allocate = allocHint(static_cast<std::size_t>(N));
		m_data = getAligned(allocate);

		for (std::size_t idx = 0; idx < static_cast<std::size_t>(N); idx++)
			new (m_data + idx) T{};

		m_allocated = allocate;
		m_used = static_cast<std::size_t>(N);
	}

	OMPVector(std::initializer_list<T> l)
	{
		if (l.size() > MAXIMUM)
			throw std::out_of_range{"Initializer is too long for OMPVector"};
		m_data = getAligned(l.size());

		std::size_t idx{0};
		for (auto &&item : l)
			new (m_data + idx++) T{item};
		m_allocated = l.size();
		m_used = m_allocated;
	}

	OMPVector(const OMPVector &other)
	{
		m_data = getAligned(other.m_allocated);

		for (std::size_t idx = 0; idx < other.m_used; idx++)
			new (m_data + idx) T{other.m_data[idx]};

		m_allocated = other.m_allocated;
		m_used = other.m_used;
	}

	OMPVector(OMPVector &&other) noexcept :
		m_data{other.m_data},
		m_allocated{other.m_allocated},
		m_used{other.m_used}
	{
		other.m_data = nullptr;
	}

	~OMPVector() noexcept
	{
		if (m_data != nullptr) {
			clear();
			freeAligned(m_data);
		}
	}

	T & at(const size_type idx)
	{
		checkRange(idx);

		return m_data[idx];
	}

	const T & at(const size_type idx) const
	{
		checkRange(idx);

		return m_data[idx];
	}

	iterator begin() noexcept
	{
		return iterator{m_data};
	}

	const_iterator begin() const noexcept
	{
		return const_iterator{m_data};
	}

	size_type capacity() const
	{
		return static_cast<size_type>(m_allocated);
	}

	const_iterator cbegin() const
	{
		return const_iterator{m_data};
	}

	const_iterator cend() const
	{
		return const_iterator{m_data + m_used};
	}

	void clear()
	{
		for (std::size_t idx = 0; idx < m_used; idx++)
			m_data[idx].~T();
		m_used = 0;
	}

	T * data()
	{
		return m_data;
	}

	const T * data() const
	{
		return m_data;
	}

	bool empty() const
	{
		return m_used == 0;
	}

	template <typename ...Args>
	void emplace_back(Args &&...args)
	{
		expandIfNeeded();

		new (m_data + m_used) T(std::forward<Args>(args)...);
		m_used++;
	}

	iterator end() noexcept
	{
		return iterator{m_data + m_used};
	}

	const_iterator end() const noexcept
	{
		return const_iterator{m_data + m_used};
	}

	size_type max_size() const
	{
		return static_cast<size_type>(MAXIMUM);
	}

	void pop_back()
	{
		if (m_used == 0)
			return;

		m_data[m_used-1].~T();
		m_used--;
	}

	void push_back(const T &v)
	{
		expandIfNeeded();

		new (m_data + m_used) T{v};
		m_used++;
	}

	void reserve(const size_type N)
	{
		checkNegative(N);

		if (static_cast<std::size_t>(N) > m_allocated)
			reallocate(m_used, static_cast<std::size_t>(N));
	}

	void resize(const size_type N)
	{
		checkNegative(N);

		reallocate(static_cast<std::size_t>(N),
			   static_cast<std::size_t>(N));

		for (size_t idx = m_used; idx < static_cast<std::size_t>(N); idx++)
			new (m_data + idx) T{};
		m_used = static_cast<std::size_t>(N);
	}

	void shrink_to_fit()
	{
		reallocate(m_used, m_used);
	}

	size_type size() const
	{
		return static_cast<size_type>(m_used);
	}

	T & operator[](const size_type idx)
	{
		return m_data[idx];
	}

	const T & operator[](const size_type idx) const
	{
		return m_data[idx];
	}

	bool operator==(const OMPVector &other) const
	{
		return m_data == other.m_data;
	}

	bool operator!=(const OMPVector &other) const
	{
		return !(*this == other);
	}

	OMPVector & operator=(const OMPVector &other)
	{
		if (m_data != nullptr) {
			clear();
			freeAligned(m_data);
		}

		m_data = getAligned(other.m_allocated);

		for (std::size_t idx = 0; idx < other.m_used; idx++)
			new (m_data + idx) T{other.m_data[idx]};

		m_allocated = other.m_allocated;
		m_used = other.m_used;

		return *this;
	}

	OMPVector & operator=(OMPVector &&other) noexcept
	{
		if (m_data != nullptr) {
			clear();
			freeAligned(m_data);
		}

		m_data = other.m_data;
		m_used = other.m_used;
		m_allocated = other.m_allocated;

		other.m_data = nullptr;

		return *this;
	}

private:
	std::size_t allocHint(const std::size_t N) const
	{
		const std::size_t hint = [N]() -> std::size_t {
			if (N == 0)
				return DEFAULT_ALLOC;
			return std::size_t(N * 1.25);
		}();

		if (hint < N)
			return MAXIMUM;
		return hint;
	}

	void checkNegative(const size_type idx);

	void checkRange(const size_type idx);

	void expandIfNeeded()
	{
		if (m_used < m_allocated)
			return;

		if (m_used == MAXIMUM)
			throw std::bad_alloc{};

		reallocate(m_used, allocHint(m_used + 1));
	}

	T * getAligned(const std::size_t N)
	{
		T *ptr = static_cast<T *>(_mm_malloc(N * sizeof(T), Alignment));
		if (ptr == nullptr)
			throw std::bad_alloc{};
		return ptr;
	}

	void freeAligned(T *ptr)
	{
		_mm_free(ptr);
	}

	void reallocate(const std::size_t items, const std::size_t allocate)
	{
		auto newData = getAligned(allocate);

		const auto last = items > m_used ? m_used : items;

		for (std::size_t idx = 0; idx < last; idx++)
			new (newData + idx) T(std::move(m_data[idx]));

		for (std::size_t idx = last; idx < m_used; idx++)
			m_data[idx].~T();

		freeAligned(m_data);

		m_data = newData;
		m_allocated = allocate;
		m_used = last;
	}

	T *m_data;
	std::size_t m_allocated;
	std::size_t m_used;

	static const std::size_t DEFAULT_ALLOC{32};
	static const std::size_t MAXIMUM{std::numeric_limits<std::size_t>::max() - 1};
};

namespace OMPVectorUtils {

template <typename T, typename S, std::size_t Alignment>
using size_type = typename OMPVector<T, S, Alignment>::size_type;

template <typename T, typename S, std::size_t Alignment>
inline
std::string makeErrorMsg(const size_type<T, S, Alignment> idx, const size_type<T, S, Alignment> used)
{
	std::string s{"Index value out of range"};

	s += " (index = " + std::to_string(idx) + ", size = " + std::to_string(used) + ")";
	return s;
}

template <typename T, typename S, std::size_t Alignment, bool IS_SIGNED>
class NegativeChecker {
};

template <typename T, typename S, std::size_t Alignment>
class NegativeChecker<T, S, Alignment, true> {
public:
	static void check(const size_type<T, S, Alignment> idx)
	{
		if (idx < 0)
			throw std::out_of_range{"Value cannot be negative"};
	}
};

template <typename T, typename S, std::size_t Alignment>
class NegativeChecker<T, S, Alignment, false> {
public:
	static void check(const size_type<T, S, Alignment>)
	{
		/* NOOP */
	}
};

template <typename T, typename S, std::size_t Alignment, bool IS_SIGNED>
class RangeChecker {
};

template <typename T, typename S, std::size_t Alignment>
class RangeChecker<T, S, Alignment, true> {
public:

	static void check(const size_type<T, S, Alignment> idx, const size_type<T, S, Alignment> used)
	{
		if (idx < 0 || idx >= used)
			throw std::out_of_range{makeErrorMsg<T, S, Alignment>(idx, used)};
	}
};

template <typename T, typename S, std::size_t Alignment>
class RangeChecker<T, S, Alignment, false> {
public:
	static void check(const size_type<T, S, Alignment> idx, const size_type<T, S, Alignment> used)
	{
		if (idx >= used)
			throw std::out_of_range{makeErrorMsg<T, S, Alignment>(idx, used)};
	}
};

} // namespace OMPVectorUtils

template <typename T, typename S, std::size_t Alignment>
void OMPVector<T, S, Alignment>::checkNegative(const size_type idx)
{
	OMPVectorUtils::NegativeChecker<T, S, Alignment, std::is_signed<size_type>::value>::check(idx);
}

template <typename T, typename S, std::size_t Alignment>
void OMPVector<T, S, Alignment>::checkRange(const size_type idx)
{
	OMPVectorUtils::RangeChecker<T, S, Alignment, std::is_signed<size_type>::value>::check(idx, m_used);
}

#endif // XFISH_OMPVECTOR_H
