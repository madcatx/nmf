#ifndef SIMULATOR_H
#define SIMULATOR_H

#include "instruction_set.h"
#include "simulation_context.h"
#include "simulation_observer.h"

namespace NMF {

template <InstructionSet ISet> class ElmigSystem;

template <Stepper S, InstructionSet ISet>
class Simulator {
};

template <InstructionSet ISet>
class Simulator<Stepper::RKCK54, ISet> {
public:
	explicit Simulator(SimulationContext &ctx);
	~Simulator();

	void step();

private:
	SimulationContext &m_ctx;
	ElmigSystem<ISet> *m_sys;
	SimulationObserver m_obs;
	rkck54_stepper m_stp;
};

template <InstructionSet ISet>
class Simulator<Stepper::ABM4, ISet> {
public:
	explicit Simulator(SimulationContext &ctx);
	~Simulator();

	void step();

private:
	SimulationContext &m_ctx;
	ElmigSystem<ISet> *m_sys;
	SimulationObserver m_obs;
	abm4_stepper m_stp;
};

} // namespace NMF

#endif // SIMULATOR_H
