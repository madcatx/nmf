#ifndef VECTORIZERS_H
#define VECTORIZERS_H

#include <echmetmodule.h>
#ifdef ECHMET_COMPILER_MSVC
	#include <intrin.h>
#else
	#include <x86intrin.h>
#endif // ECHMET_COMPILER_MSVC
#include <functional>

#include "instruction_set.h"

namespace NMF {

template <typename T>
T * alignedAlloc(const size_t count)
{
	return static_cast<T *>(_mm_malloc(count * sizeof(T), 64));
}

template <size_t Idx, typename Unrollee, typename... Args>
class Unroller {
public:
	static void unroll(Args... args)
	{
		Unrollee::call(Idx - 1, args...);
		Unroller<Idx - 1, Unrollee, Args...>::unroll(args...);
	}
};

template <typename Unrollee, typename... Args>
class Unroller<1, Unrollee, Args...> {
public:
	static void unroll(Args... args)
	{
		Unrollee::call(0, args...);
	}
};

template <InstructionSet ISet>
class VectorizedLoopBlock {
public:
	typedef void type;
};

#ifdef NMF_SSE2_ENABLED
template <>
class VectorizedLoopBlock<InstructionSet::SSE2> {
public:
	typedef __m128d type;
};
#endif // NMF_SSE2_ENABLED

#ifdef NMF_AVX_ENABLED
template <>
class VectorizedLoopBlock<InstructionSet::AVX> {
public:
	typedef __m256d type;
};
#endif // NMF_AVX_ENABLED

#ifdef NMF_FMA3_ENABLED
template <>
class VectorizedLoopBlock<InstructionSet::FMA3> {
public:
	typedef __m256d type;
};
#endif // NMF_FMA3_ENABLED

#ifdef NMF_AVX512_ENABLED
template <>
class VectorizedLoopBlock<InstructionSet::AVX512> {
public:
	typedef __m512d type;
};
#endif // NMF_AVX512_ENABLED

template <InstructionSet ISet, typename DataType>
class VFunctions {
public:
	typedef typename VectorizedLoopBlock<ISet>::type VType;
	typedef double VD[1];

	static ECHMET_FORCE_INLINE VType load(const DataType *);
	static ECHMET_FORCE_INLINE void store(DataType *, VType);
	static ECHMET_FORCE_INLINE VType add(VType, VType);
	static ECHMET_FORCE_INLINE VType div(VType, VType);
	static ECHMET_FORCE_INLINE VType mul(VType, VType);
};

#ifdef NMF_SSE2_ENABLED
template <>
class VFunctions<InstructionSet::SSE2, double> {
public:
	typedef typename VectorizedLoopBlock<InstructionSet::SSE2>::type VType;
	typedef double ECHMET_ALIGNED_BEF_16 VD[2] ECHMET_ALIGNED_AFT_16;

	static ECHMET_FORCE_INLINE VType load(const double *src) { return *(VType *)src; }
	static ECHMET_FORCE_INLINE void store(double *dst, VType src) { _mm_store_pd(dst, src); }
	static ECHMET_FORCE_INLINE VType add(VType a, VType b) { return _mm_add_pd(a, b); }
	static ECHMET_FORCE_INLINE VType div(VType a, VType b) { return _mm_div_pd(a, b); }
	static ECHMET_FORCE_INLINE VType move(const VType src) { return _mm_movedup_pd(src); }
	static ECHMET_FORCE_INLINE VType move(const VD src) { return _mm_movedup_pd(*(const VType*)src); }
	static ECHMET_FORCE_INLINE VType mul(VType a, VType b) { return _mm_mul_pd(a, b); }
	static ECHMET_FORCE_INLINE VType zero() { return _mm_setzero_pd(); }
	static ECHMET_FORCE_INLINE void zeroize(VD dst) { _mm_store_pd(dst, _mm_setzero_pd()); }
};
#endif // NMF_SSE2_ENABLED

#ifdef NMF_AVX_ENABLED
template <>
class VFunctions<InstructionSet::AVX, double> {
public:
	typedef typename VectorizedLoopBlock<InstructionSet::AVX>::type VType;
	typedef double ECHMET_ALIGNED_BEF_32 VD[4] ECHMET_ALIGNED_AFT_32;

	static ECHMET_FORCE_INLINE VType load(const double *src) { return *(VType *)src; }
	static ECHMET_FORCE_INLINE void store(double *dst, VType src) { _mm256_store_pd(dst, src); }
	static ECHMET_FORCE_INLINE VType add(VType a, VType b) { return _mm256_add_pd(a, b); }
	static ECHMET_FORCE_INLINE VType div(VType a, VType b) { return _mm256_div_pd(a, b); }
	static ECHMET_FORCE_INLINE VType move(const VType src) { return _mm256_movedup_pd(src); }
	static ECHMET_FORCE_INLINE VType move(const VD src) { return _mm256_movedup_pd(*(const VType*)&src); }
	static ECHMET_FORCE_INLINE VType mul(VType a, VType b) { return _mm256_mul_pd(a, b); }
	static ECHMET_FORCE_INLINE VType zero() { return _mm256_setzero_pd(); }
	static ECHMET_FORCE_INLINE void zeroize(VD dst) { _mm256_store_pd(dst, _mm256_setzero_pd()); }
};
#endif // NMF_AVX_ENABLED

#ifdef NMF_FMA3_ENABLED
template <>
class VFunctions<InstructionSet::FMA3, double> {
public:
	typedef typename VectorizedLoopBlock<InstructionSet::FMA3>::type VType;
	typedef double ECHMET_ALIGNED_BEF_32 VD[4] ECHMET_ALIGNED_AFT_32;

	static ECHMET_FORCE_INLINE VType load(const double *src) { return *(VType *)src; }
	static ECHMET_FORCE_INLINE void store(double *dst, VType src) { _mm256_store_pd(dst, src); }
	static ECHMET_FORCE_INLINE VType add(VType a, VType b) { return _mm256_add_pd(a, b); }
	static ECHMET_FORCE_INLINE VType div(VType a, VType b) { return _mm256_div_pd(a, b); }
	static ECHMET_FORCE_INLINE VType move(const VType src) { return _mm256_movedup_pd(src); }
	static ECHMET_FORCE_INLINE VType move(const VD src) { return _mm256_movedup_pd(*(const VType*)&src); }
	static ECHMET_FORCE_INLINE VType mul(VType a, VType b) { return _mm256_mul_pd(a, b); }
	static ECHMET_FORCE_INLINE VType zero() { return _mm256_setzero_pd(); }
	static ECHMET_FORCE_INLINE void zeroize(VD dst) { _mm256_store_pd(dst, _mm256_setzero_pd()); }
};
#endif // NMF_FMA3_ENABLED

#ifdef NMF_AVX512_ENABLED
template <>
class VFunctions<InstructionSet::AVX512, double> {
public:
	typedef typename VectorizedLoopBlock<InstructionSet::AVX512>::type VType;
	typedef double ECHMET_ALIGNED_BEF_64 VD[8] ECHMET_ALIGNED_AFT_64;

	static ECHMET_FORCE_INLINE VType load(const double *src) { return *(VType *)src; }
	static ECHMET_FORCE_INLINE void store(double *dst, VType src) { _mm512_store_pd(dst, src); }
	static ECHMET_FORCE_INLINE VType add(VType a, VType b) { return _mm512_add_pd(a, b); }
	static ECHMET_FORCE_INLINE VType div(VType a, VType b) { return _mm512_div_pd(a, b); }
	static ECHMET_FORCE_INLINE VType move(const VType src) { return _mm512_movedup_pd(src); }
	static ECHMET_FORCE_INLINE VType move(const VD src) { return _mm512_movedup_pd(*(const VType*)&src); }
	static ECHMET_FORCE_INLINE VType mul(VType a, VType b) { return _mm512_mul_pd(a, b); }
	static ECHMET_FORCE_INLINE VType zero() { return _mm512_setzero_pd(); }
	static ECHMET_FORCE_INLINE void zeroize(VD dst) { _mm512_store_pd(dst, _mm512_setzero_pd()); }
};
#endif // NMF_AVX512_ENABLED

template <InstructionSet ISet, typename DataType, typename LoopCtx,
	  template <typename> class SingleShotFunc, template <typename, InstructionSet> class LoopFunc>
class VectorizedLoop {
private:
	typedef std::underlying_type<InstructionSet>::type NBlocks;
public:
	static void call(const size_t from, const size_t to, LoopCtx &ctx)
	{
		const size_t idx = processUnaligned(from, ctx);

		const size_t alignedTo = alignedCount(to);
		LoopFunc<DataType, ISet>::call(idx, alignedTo, ctx);

		processUnaligned(alignedTo, to, ctx);
	}
private:
	static size_t alignedCount(const size_t N)
	{
		return N - (N % Alignment<ISet>::value);
	}

	static size_t processUnaligned(const size_t from, LoopCtx &ctx)
	{
		const size_t to = from + Alignment<ISet>::value - (from % Alignment<ISet>::value);

		for (size_t idx = from; idx < to; idx++)
			SingleShotFunc<DataType>::call(idx, ctx);

		return to;
	}

	static void processUnaligned(const size_t from, const size_t to, LoopCtx &ctx)
	{
		for (size_t idx = from; idx < to; idx++)
			SingleShotFunc<DataType>::call(idx, ctx);
	}
};

} // namespace NMF

#endif // VECTORIZERS_H

