#include "simulator.h"

#define NMF_SSE2_ENABLED
#define NMF_AVX_ENABLED
#define NMF_FMA3_ENABLED
#include "elmig_system.h"

namespace NMF {

template <>
Simulator<Stepper::ABM4, InstructionSet::FMA3>::Simulator(SimulationContext &ctx) :
	m_ctx{ctx},
	m_sys{new ElmigSystem<InstructionSet::FMA3>{m_ctx}},
	m_obs{SimulationObserver{*m_ctx.calcSpaceAdaptor}}
{}

template <>
Simulator<Stepper::ABM4, InstructionSet::FMA3>::~Simulator()
{
	delete m_sys;
}

template <>
void Simulator<Stepper::ABM4, InstructionSet::FMA3>::step()
{
	if (m_ctx.t + m_ctx.dt > m_ctx.tStop)
		m_ctx.dt = m_ctx.tStop - m_ctx.t;

	m_stp.do_step(*m_sys, m_ctx.totalConcentrations, m_ctx.t, m_ctx.dt);
	m_ctx.t += m_ctx.dt;
	m_obs(m_ctx);
}

} // namespace NMF
