#ifndef INSTRUCTION_SET_H
#define INSTRUCTION_SET_H

#include <cstddef>

namespace NMF {

enum class InstructionSet {
	SSE2,
	AVX,
	FMA3,
	AVX512
};

template <InstructionSet>
class Alignment {};

template <>
class Alignment<InstructionSet::SSE2> {
public:
	static const size_t value{2};
};

template <>
class Alignment<InstructionSet::AVX> {
public:
	static const size_t value{4};
};

template <>
class Alignment<InstructionSet::FMA3> {
public:
	static const size_t value{4};
};

template <>
class Alignment<InstructionSet::AVX512> {
public:
	static const size_t value{8};
};

} // namespace NMF

#endif // INSTRUCTION_SET_H
