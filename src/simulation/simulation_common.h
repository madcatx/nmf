#ifndef SIMULATION_COMMON_H
#define SIMULATION_COMMON_H

#include <nmf.h>
#include "ompvector.h"

#include <echmetsyscomp.h>
#include <echmetcaes.h>
#include <echmetionprops.h>
#include <stdexcept>
#include <tuple>
#include <vector>

#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/external/openmp/openmp.hpp>

/* Some boost horrifiction */
namespace boost { namespace numeric { namespace odeint {
template <class V, class A>
struct is_resizeable<OMPVector<V,A>> : std::true_type{};
}}}

namespace ndf = boost::numeric::odeint;

namespace NMF {

using state_type = OMPVector<double, int>;
using OMPSize = state_type::size_type;
using CalculatedPropertiesVec = std::vector<ECHMET::SysComp::CalculatedProperties>;
using EqSolVec = std::vector<std::tuple<ECHMET::CAES::Solver *, ECHMET::RealVec *, double *>>;
using IonCtxVec = std::vector<ECHMET::IonProps::ComputationContext *>;

using abm4_stepper = ndf::adams_bashforth_moulton<4, state_type, double, state_type, double, ndf::openmp_range_algebra>;

using rkck54_stepper_type = ndf::runge_kutta_cash_karp54<state_type, double, state_type, double, ndf::openmp_range_algebra>;
using rkck54_stepper = typename ndf::result_of::make_controlled<rkck54_stepper_type>::type;

class SimulationError : public std::exception {
public:
	SimulationError(SimulationFailure fail) :
		m_fail{fail}
	{}

	SimulationFailure error() const noexcept
	{
		return m_fail;
	}

private:
	const SimulationFailure m_fail;
};

/* Simple structs that pack together data that are used togheter in some steps of the calculation */

class IFPack {
public:
	size_t concIdx;		/* <! IonicForm's index into the concentrations array */
	double totalCharge;	/* <! Total electric charge of the IonicForm */
};

/* Ionic forms data grouped by constituent */
class RDCtuentIFsPack {
public:
	class IFPack {
	public:
		size_t concIdx;    /*<! IonicForm index into the ionicConcentrations array */
		size_t mobIdx;     /*<! IonicForm's index into the ionicMobilities array */
		double xfrMult;
		double sgnCharge;
	};

	RDCtuentIFsPack(ECHMET::SysComp::IonicForm *const *ifs, const size_t size,
			const std::vector<double> &xfrMults, const std::vector<double> &sgnCharges) :
		ionForms{mkPack(ifs, size, xfrMults, sgnCharges)}
	{
	}

	const std::vector<IFPack> ionForms;

private:
	auto mkPack(const ECHMET::SysComp::IonicForm *const *ifs, const size_t size,
		    const std::vector<double> &xfrMults, const std::vector<double> &sgnCharges) -> std::vector<IFPack> {
		assert(size == xfrMults.size() && size == sgnCharges.size());

		std::vector<IFPack> ionForms(size);

		for (size_t idx = 0; idx < size; idx++) {
			const auto iF = ifs[idx];
			ionForms[idx].concIdx = iF->ionicConcentrationIndex;
			ionForms[idx].mobIdx = iF->ionicMobilityIndex;
			ionForms[idx].xfrMult = xfrMults[idx];
			ionForms[idx].sgnCharge = sgnCharges[idx];
		}

		return ionForms;
	}
};

class RDCalcPropsPack {
public:
	RDCalcPropsPack() :
		ionicConcentrations{nullptr},
		ionicMobilities{nullptr}
	{}

	RDCalcPropsPack(const ECHMET::RealVec *concs, const ECHMET::RealVec *mobs) :
		ionicConcentrations{concs->cdata()},
		ionicMobilities{mobs->cdata()}
	{}

	const double *const ECHMET_RESTRICT_PTR ionicConcentrations;
	const double *const ECHMET_RESTRICT_PTR ionicMobilities;
};

} // namespace NMF

#endif // SIMULATION_COMMON_H
