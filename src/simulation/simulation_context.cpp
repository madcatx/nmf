#include "simulation_context.h"

namespace NMF {

SimulationContext::SimulationContext(SimulationContext &&other) noexcept :
	isValid{other.isValid},
	updateInterval{other.updateInterval},
	tStop{other.tStop},
	t{other.t},
	dt{other.dt},

	totalConcentrations{std::move(other.totalConcentrations)},
	totalConcentrationsPrev{std::move(other.totalConcentrationsPrev)},
	effectiveMobilities{std::move(other.effectiveMobilities)},
	diffusionCoefficients{std::move(other.diffusionCoefficients)},
	calcPropsVec{std::move(other.calcPropsVec)},

	dx{other.dx},
	capillaryLength{other.capillaryLength},

	constantForceValue{other.constantForceValue},
	constantForce{other.constantForce},

	conductivityRawArray{other.conductivityRawArray},
	electricFieldRawArray{other.electricFieldRawArray},
	areaRawArray{other.areaRawArray},
	blockResistance{other.blockResistance},

	NCO{other.NCO},
	NIF{other.NIF},
	NCells{other.NCells},

	firstCell{other.firstCell},
	lastCell{other.lastCell},

	wallsCompromisedCtr{other.wallsCompromisedCtr},

	diffusionModifierCoeff{other.diffusionModifierCoeff},

	chemSystem{other.chemSystem},
	solverCtx{other.solverCtx},

	eqSolPacks{std::move(other.eqSolPacks)},
	ionCtxPacks{std::move(other.ionCtxPacks)},

	constituentsRaw{other.constituentsRaw},
	ionicFormsInfo{std::move(other.ionicFormsInfo)},
	constituentIonicFormsRaw{std::move(other.constituentIonicFormsRaw)},
	ionicFormsAbsTotalCharge{std::move(other.ionicFormsAbsTotalCharge)},

	useFullSolver{other.useFullSolver},
	corrs{other.corrs},
	correctDH{other.correctDH},
	correctOF{other.correctOF},

	current{other.current},
	voltage{other.voltage},
	H{other.H},
	invH{other.invH},
	invHH{other.invHH},

	eofMode{other.eofMode},
	baseEofValue{other.baseEofValue},

	NThreads{other.NThreads},
	tolerance{other.tolerance},

	spaceAdaptationEnabled{other.spaceAdaptationEnabled},
	calcSpaceAdaptor{other.calcSpaceAdaptor},

	avgTimePerIter{other.avgTimePerIter}
{
	other.conductivityRawArray = nullptr;
	other.electricFieldRawArray = nullptr;
	other.areaRawArray = nullptr;
	other.blockResistance = nullptr;

	other.solverCtx = nullptr;

	other.constituentsRaw = nullptr;
	other.calcSpaceAdaptor = nullptr;
}

SimulationContext & SimulationContext::operator=(SimulationContext &&other) noexcept
{
	isValid = other.isValid;
	updateInterval = other.updateInterval;
	tStop = other.tStop;
	t = other.t;
	dt = other.dt;

	totalConcentrations = std::move(other.totalConcentrations);
	totalConcentrationsPrev = std::move(other.totalConcentrationsPrev);
	effectiveMobilities = std::move(other.effectiveMobilities);
	diffusionCoefficients = std::move(other.diffusionCoefficients);
	calcPropsVec = std::move(other.calcPropsVec);

	dx = other.dx;
	capillaryLength = other.capillaryLength;

	constantForceValue = other.constantForceValue;
	constantForce = other.constantForce;

	conductivityRawArray = other.conductivityRawArray;
	electricFieldRawArray = other.electricFieldRawArray;
	areaRawArray = other.areaRawArray;
	blockResistance = other.blockResistance;

	NCO = other.NCO;
	NIF = other.NIF;
	NCells = other.NCells;

	firstCell = other.firstCell;
	lastCell = other.lastCell;

	wallsCompromisedCtr = other.wallsCompromisedCtr;

	diffusionModifierCoeff = other.diffusionModifierCoeff;

	chemSystem = other.chemSystem;
	solverCtx = other.solverCtx;

	eqSolPacks = std::move(other.eqSolPacks);
	ionCtxPacks = std::move(other.ionCtxPacks);

	constituentsRaw = other.constituentsRaw;
	ionicFormsInfo = std::move(other.ionicFormsInfo);
	constituentIonicFormsRaw = std::move(other.constituentIonicFormsRaw);
	ionicFormsAbsTotalCharge = std::move(other.ionicFormsAbsTotalCharge);

	useFullSolver = other.useFullSolver;
	corrs = other.corrs;
	correctDH = other.correctDH;
	correctOF = other.correctOF;

	current = other.current;
	voltage = other.voltage;
	H = other.H;
	invH = other.invH;
	invHH = other.invHH;

	eofMode = other.eofMode;
	baseEofValue = other.baseEofValue;

	NThreads = other.NThreads;
	tolerance = other.tolerance;

	spaceAdaptationEnabled = other.spaceAdaptationEnabled;
	calcSpaceAdaptor = other.calcSpaceAdaptor;

	avgTimePerIter = other.avgTimePerIter;

	other.conductivityRawArray = nullptr;
	other.electricFieldRawArray = nullptr;
	other.areaRawArray = nullptr;
	other.blockResistance = nullptr;

	other.solverCtx = nullptr;

	other.constituentsRaw = nullptr;
	other.calcSpaceAdaptor = nullptr;

	return *this;
}

} // namespace NMF
