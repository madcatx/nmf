#ifndef SIMULATION_OBSERVER_H
#define SIMULATION_OBSERVER_H

#include "simulation_common.h"
#include "simulation_context.h"

namespace NMF {

class CalcSpaceAdaptor;

class SimulationObserver {
public:
	SimulationObserver(CalcSpaceAdaptor &adaptor);
	SimulationObserver(const SimulationObserver &) = delete;
        SimulationObserver(SimulationObserver && other) noexcept;
	SimulationObserver & operator=(const SimulationObserver &) = delete;

	void operator()(SimulationContext &ctx);

private:
	double average(const std::vector<double> &v);

	double timeLast;
	std::vector<double> elapsed;
	std::vector<double> dts;
	size_t timeinfoBufIdx;
#ifdef LIBNMF_USE_WINAPI
        int64_t qpcFrequency;
#endif // LIBNMF_USE_WINAPI

	CalcSpaceAdaptor &h_adaptor;
};

} // namespace NMF

#endif // SIMULATION_OBSERVER_H
