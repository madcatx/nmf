#include "simulation_observer.h"

#include "calcspace_adaptor.h"

#include <cmath>
#include <numeric>

#ifdef LIBNMF_USE_WINAPI
#include <Windows.h>
#endif // LIBNMF_USE_WINAPI

namespace NMF {

static const size_t TIMEINFO_HISTORY_BUF_SIZE{10};

static
auto getCurrentTime()
{
#ifdef LIBNMF_USE_WINAPI
	LARGE_INTEGER timeVal;
	if (!QueryPerformanceCounter(&timeVal))
		throw SimulationError{SimulationFailure::INTERNAL};
	return int64_t(timeVal.QuadPart);
#else
	struct timespec time;
	if (clock_gettime(CLOCK_MONOTONIC, &time))
		throw SimulationError{SimulationFailure::INTERNAL};
	return time;
#endif // LIBNMF_USE_WINAPI
}


#ifdef LIBNMF_USE_WINAPI
static
double internalTimeToSecs(const int64_t &time, const int64_t &qpcFrequency)
{
	return double(time) / qpcFrequency;
}

static
double internalTimeToSecs(const DWORD time)
{
	return double(time) / 1000.0;
}
#else
static
double internalTimeToSecs(const struct timespec &time)
{
	return time.tv_sec + (time.tv_nsec / 1.0e9);
}
#endif // LIBNMF_USE_WINAPI

SimulationObserver::SimulationObserver(CalcSpaceAdaptor &adaptor) :
	timeinfoBufIdx{0},
	h_adaptor{adaptor}
{
	elapsed.resize(TIMEINFO_HISTORY_BUF_SIZE);
	dts.resize(TIMEINFO_HISTORY_BUF_SIZE);

#ifdef LIBNMF_USE_WINAPI
        LARGE_INTEGER freq;
        if (!QueryPerformanceFrequency(&freq))
            throw SimulationError{SimulationFailure::INTERNAL};
        qpcFrequency = freq.QuadPart;
#endif // LIBNMF_USE_WINAPI

#ifdef LIBNMF_USE_WINAPI
	timeLast = internalTimeToSecs(getCurrentTime(), qpcFrequency);
#else
        timeLast = internalTimeToSecs(getCurrentTime());
#endif // LIBNMF_USE_WINAPI
}

SimulationObserver::SimulationObserver(SimulationObserver &&other) noexcept :
        timeLast{other.timeLast},
        elapsed{std::move(other.elapsed)},
        dts{std::move(other.dts)},
        timeinfoBufIdx{other.timeinfoBufIdx},
#ifdef LIBNMF_USE_WINAPI
        qpcFrequency{other.qpcFrequency},
#endif // LIBNMF_USE_WINAPI
        h_adaptor{other.h_adaptor}
{
}

void SimulationObserver::operator()(SimulationContext &ctx)
{
	if (ctx.spaceAdaptationEnabled)
		h_adaptor(ctx);
}

double SimulationObserver::average(const std::vector<double> &v)
{
	return std::accumulate(v.cbegin(), v.cend(), 0.0) / v.size();
}

} // namespace NMF
