libNMF
===

Introduction
---
libNMF is an independently developed 1D capillary electrophoresis simulator. The solver uses [ECHMETCoreLibs](https://github.com/echmet/ECHMETCoreLibs) to calculate concentration equilibrium. As such, libNMF can simulate systems with electrophoretic systems with complex-forming equilibria. In terms of system composition and allowed complex-forming interactions, libNMF has the same limitations as [PeakMaster NG](https://github.com/echmet/PeakMasterNG).

Building
---
The following tools are libraries are required to build libNMF

- C++14-aware compiler
- [ECHMETCoreLibs](https://github.com/echmet/ECHMETCoreLibs)
- [odeint](http://www.odeint.com) - part of the [Boost](https://www.boost.org) library
- [CMake](https://cmake.org/) 3.9 or higher

### Linux/UNIX
In the terminal, `cd` to the directory with libNMF source code and issue the following series of commands

    mkdir build
    cd build
    cmake .. -DCMAKE_BUILD_TYPE=Release -DBOOST_DIR=<path_to_your_boost_installation> -DECHMET_CORE_LIBS_DIR=<path_to_your_ECHMETCoreLibs_installation>
    make
    make install

Note that paths to Boost and ECHMETCoreLibs must be set explicitly only if you do not have a system-wide installation of the respective libraries.

### Windows
On Windows, both [MinGW](https://sourceforge.net/projects/mingw-w64/) and Microsoft Visual C++ Compiler (MSVC) can be used to build libNMF. Use CMake to generate project files for your compiler of choice. Make sure that `BOOST_DIR` and `ECHMET_CORE_LIBS_DIR` variables are set to point to paths with your Boost and ECHMETCoreLibs installations, respectively. Note that at least MSVC 14 (part of Microsoft Visual Studio 2015) is required.

#### Remarks specific for MSVC
libNMF uses OpenMP for parallel processing. Given the implementation of OpenMP in current versions of MinGW, building libNMF with MSVC instead of MinGW may provide considerably better performance. MSVC is, however, limited to OpenMP 2.0 and therefore cannot build the odeint library. [This](https://github.com/boostorg/odeint/issues/31) patch may be used to make odeint buildable with MSVC. Furthermore, OpenMP library used by MSVC uses `dynamic, 1` scheduling policy by default. This is quite inefficient with combination of libNMF and odeint. To maximize performance, one may either:

- Set the `OMP_SCHEDULE="static"` environment variable

or

- Replace the `runtime` scheduling directive in `openmp_range_algebra.hpp` file of the odeint library with `static` directive everywhere in that file.

Licensing
---
libNMF project is distributed under the terms of **The GNU General Public License v3** (GNU GPLv3). See the enclosed `LICENSE` file for details.
