#ifndef LIBNMF_H
#define LIBNMF_H

#include <echmetelems.h>
#include <echmetsyscomp.h>

#include <cassert>
#include <stdexcept>
#include <vector>


#define NMF_K_U_DT "dt"
#define NMF_K_U_ITER_AVG_TIME "iter-avg-time"
#define NMF_K_U_TIME "time"

#define NMF_K_IU_CFTYPE "cf_type"
#define NMF_K_IU_NUM_CELLS "num_cells"
#define NMF_K_IU_NUM_CONSTITUENTS "num_constituents"
#define NMF_K_IU_DX "dx"
#define NMF_K_IU_COND_PROFILE "cond_profile"
#define NMF_K_IU_PH_PROFILE "pH_profile"
#define NMF_K_IU_PROFILES "profiles"
#define NMF_K_IU_CONSTITUENT_NAMES "constituent_names"
#define NMF_K_IU_FIRST_CELL "first-cell"
#define NMF_K_IU_LAST_CELL "last-cell"
#define NMF_K_IU_CURRENT "current"
#define NMF_K_IU_VOLTAGE "voltage"

#define NMF_K_T_REASON "reason"

namespace NMF {

ECHMET_ST_ENUM(RetCode) {
	OK = 0,
	E_NO_MEMORY = 0x1,			/*!< Insufficient memory to complete operation. */
	E_INVALID_ARGUMENT = 0x2,		/*!< Argument passed to a function was invalid. */
	E_NOT_IMPLEMENTED = 0x3,		/*!< Requested operation is currently not implemented */
	E_WORKER_NOT_INITIALIZED = 0x4,		/*!< Simulation worker has not been initialized prior to
						     calling <tt>start()</tt> */
	DONE = 0x5,				/*!< Simulation is already done. Increase <tt>tEnd</tt>
						     to allow the simulation to proceed */
	E_RUNTIME_ERROR = 0x6,			/*!< Runtime error occured during simulation */
	E_CPU_UNSUPPORTED = 0x7,		/*!< System CPU is not supported by the computation code */
	E_BUSY = 0x8,				/*!< Simulation is already running */
	E_CANNOT_ZERO_STEP = 0x9,		/*!< Cannot calculate the "zero step" (initial conditions)
						     of the simulation */
	NOT_RUNNING = 0xA,			/*!< Simulation is not running */
	ENUM_FORCE_INT32_SIZE(NMFRetCode)
};

ECHMET_ST_ENUM(EventType) {
	STARTED = 0,
	PAUSED = 0x1,
	INITIALIZED = 0x2,
	DATA_UPDATED = 0x3,
	TERMINATED = 0x4,
	ENUM_FORCE_INT32_SIZE(NMFEvent)
};

ECHMET_ST_ENUM(DataType) {
	INT32 = 0,
	DOUBLE = 0x1,
	SIZE_T = 0x2,
	BOOL = 0x3,
	DOUBLE_VEC = 0x4,
	PROFILES = 0x5,
	STRING = 0x6,
	STRINGS = 0x7,
	CF_TYPE = 0x8,
	SIM_FAIL = 0x9,
	ENUM_FORCE_INT32_SIZE(NMFDataType)
};

ECHMET_ST_ENUM(ConstantForce) {
	VOLTAGE = 0,
	CURRENT = 0x1,
	ENUM_FORCE_INT32_SIZE(NMFConstantForce)
};

ECHMET_ST_ENUM(ElectroosmoticFlowMode) {
	MOBILITY = 0,
	VELOCITY = 0x1,
	ENUM_FORCE_INT32_SIZE(NMFElectroosmoticFlowMode)
};

ECHMET_ST_ENUM(InitializationFailure) {
	NUM_CELLS = 0,
	CAPILLARY_LENGTH = 0x1,
	INVALID_CONSTITUENT = 0x2,
	INVALID_COMPLEXATION = 0x3,
	DUPLICIT_CONSTITUENT = 0x4,
	SYSTEM_TOO_LARGE = 0x5,
	PROFILES = 0x6,
	COMPOSITION_UNSPECIFIED = 0x7,
	NO_CONSTITUENTS = 0x8,
	MISMATCHING_PROFILES = 0x9,
	FIRST_LAST_CELL = 0xA,
	UNKNOWN = 0x7FFFFFE,
	ENUM_FORCE_INT32_SIZE(NMFInitializationFailure)
};

ECHMET_ST_ENUM(ProfileType) {
	CONDUCTIVITY = 0,
	PH = 0x1,
	CONCENTRATION = 0x2,
	ENUM_FORCE_INT32_SIZE(NMFProfileType)
};

ECHMET_ST_ENUM(SimulationFailure) {
	CANNOT_ADJUST_DT = 0,
	INTERNAL = 0x1,
	CANNOT_SOLVE_EQUILIBRIUM = 0x2,
	CANNOT_CORRECT_MOBILITIES = 0x3,
	ENUM_FORCE_INT32_SIZE(NMFSimulationFailure)
};

ECHMET_ST_ENUM(StartFailure) {
	T_STOP = 0,				/*!< Invalid stop time */
	DT = 0x1,				/*!< Invalid dt */
	TOLERANCE = 0x2,			/*!< Invalid tolerance */
	UPDATE_INTERVAL = 0x3,			/*!< Invalid update interval */
	FIRST_LAST_CELL = 0x4,			/*!< Invalid position of first or last cell of calculation space */
	ENUM_FORCE_INT32_SIZE(NMFStartFailure)
};

ECHMET_ST_ENUM(Stepper) {
	RKCK54 = 0,
	ABM4 = 0x1,
	ENUM_FORCE_INT32_SIZE(NMFStepper)
};

class Profiles {
public:
	virtual const double * ECHMET_CC data(const int32_t cell) const = 0;
	virtual void ECHMET_CC destroy() = 0;
	virtual int32_t ECHMET_CC numCells() const = 0;
	virtual int32_t ECHMET_CC numRows() const = 0;
	virtual double ECHMET_CC operator()(const int32_t rowIdx, const int32_t cellIdx) const = 0;

protected:
	virtual ~Profiles() = 0;
};

class Strings {
public:
	virtual void destroy() = 0;
	virtual const ECHMET::FixedString * ECHMET_CC elem(const size_t idx) const = 0;
	virtual size_t ECHMET_CC size() const = 0;

protected:
	virtual ~Strings() = 0;
};

class Item {
public:
	DataType type;
	void *data;
};
IS_POD(Item)

class ItemMap {
public:
	virtual const Item & ECHMET_CC get(const char *key) const = 0;
	virtual bool ECHMET_CC has(const char *key) const = 0;

protected:
	virtual ~ItemMap() = 0;
};

template <typename T>
inline
bool typeMatches(const DataType type)
{
	(void)type;

	return false;
}

template <>
inline
bool typeMatches<int32_t>(const DataType type)
{
	return type == DataType::INT32;
}

template <>
inline
bool typeMatches<double>(const DataType type)
{
	return type == DataType::DOUBLE;
}

template <>
inline
bool typeMatches<size_t>(const DataType type)
{
	return type == DataType::SIZE_T;
}

template <>
inline
bool typeMatches<bool>(const DataType type)
{
	return type == DataType::BOOL;
}

template <>
inline
bool typeMatches<ECHMET::RealVec *>(const DataType type)
{
	return type == DataType::DOUBLE_VEC;
}

template <>
inline
bool typeMatches<Profiles *>(const DataType type)
{
	return type == DataType::PROFILES;
}

template <>
inline
bool typeMatches<ECHMET::FixedString *>(const DataType type)
{
	return type == DataType::STRING;
}

template <>
inline
bool typeMatches<Strings *>(const DataType type)
{
	return type == DataType::STRINGS;
}

template <>
inline
bool typeMatches<ConstantForce>(const DataType type)
{
	return type == DataType::CF_TYPE;
}

template <>
inline
bool typeMatches<SimulationFailure>(const DataType type)
{
	return type == DataType::SIM_FAIL;
}

class TypeMismatchError : public std::runtime_error {
public:
	explicit TypeMismatchError() :
		std::runtime_error{"Type stored in the item does not match"}
	{}

	using std::runtime_error::what;
};

template <typename T, bool IsPointer>
class payload_caster {};

template <typename T>
class payload_caster<T, false> {
public:
	using RT = typename std::decay<T>::type;

	static RT cast(void *payload)
	{
		return *static_cast<T *>(payload);
	}
};

template <typename T>
class payload_caster<T, true> {
public:
	using RT = typename std::decay<T>::type;

	static RT cast(void *payload)
	{
		return static_cast<RT>(payload);
	}
};

class Packet {
public:
	Packet() {}
	Packet(const Packet &) = delete;
	Packet(Packet &&) = delete;

	template <typename T>
	typename payload_caster<T, std::is_pointer<T>::value>::RT get(const char *key) const
	{
		assert(itemMap()->has(key));

		const auto &item = itemMap()->get(key);
		if (!typeMatches<T>(item.type))
			throw TypeMismatchError{};

		return payload_caster<T, std::is_pointer<T>::value>::cast(item.data);
	}

	bool has(const char *key) const
	{
		return itemMap()->has(key);
	}

protected:
	virtual ~Packet() = 0;
	virtual const ItemMap * ECHMET_CC itemMap() const = 0;
};

class Event {
public:
	Event() {}
	Event(const Event &) = delete;
	Event(Event &&) = delete;

	virtual EventType type() const = 0;
	virtual const Packet & packet() const = 0;

protected:
	virtual ~Event() = 0;
};

class IFConcPair {
public:
	ECHMET::FixedString *name;
	double concentration;
};
IS_POD(IFConcPair)

typedef ECHMET::Vec<IFConcPair> IFConcPairVec;

class Initialization {
public:
	ECHMET::SysComp::InConstituentVec *composition;
	Profiles *profiles;

	int32_t cells;

	ConstantForce constantForce;
	double constantForceValue;

	double capillaryLength;
	double capillaryDiameter;

	ECHMET::NonidealityCorrections corrections;

	int32_t firstCell;
	int32_t lastCell;
	bool adaptCalcSpace;
};
IS_POD(Initialization)

class ProfileSlice {
public:
	ECHMET::FixedString *name;
	ProfileType type;
	int32_t cell;
	double x;
	double value;
	double effectiveMobility;

	IFConcPairVec *concentrations;
};
IS_POD(ProfileSlice)

class Start {
public:
	double tStop;

	ConstantForce constantForce;
	double constantForceValue;

	double dt;
	int32_t tolerance;

	ElectroosmoticFlowMode eofMode;
	double eofValue;

	double updateInterval;

	int32_t firstCell;
	int32_t lastCell;
	bool adaptCalcSpace;
};
IS_POD(Start)

class Worker {
public:
	virtual RetCode ECHMET_CC currentState(Packet *&packet) = 0;
	virtual void ECHMET_CC destroy() = 0;
	virtual RetCode ECHMET_CC initialize(const Initialization &init, InitializationFailure &fail) = 0;
	virtual bool ECHMET_CC isRunning() const = 0;
	virtual RetCode ECHMET_CC pause() = 0;
	virtual RetCode ECHMET_CC profileSlice(const ProfileType type, const ECHMET::FixedString *name, const double x, ProfileSlice &slice) = 0;
	virtual RetCode ECHMET_CC start(const Start &params, StartFailure &fail) = 0;
	virtual Stepper ECHMET_CC stepper() const = 0;

protected:
	virtual ~Worker() = 0;
};

typedef void (ECHMET_CC * EventHandlerFunc)(Worker &worker, const Event &, void *context);


extern "C" {

ECHMET_API RetCode ECHMET_CC changeStepper(const Stepper stepper, Worker *&worker);
ECHMET_API const char * ECHMET_CC initFailToString(const InitializationFailure fail);
ECHMET_API RetCode ECHMET_CC makeProfiles(const ECHMET::RealVec *const *concentrations, const size_t numConcentrations, Profiles *&profiles);
ECHMET_API RetCode ECHMET_CC makeWorker(Worker *&worker, const Stepper stepper, const EventHandlerFunc handler, void *context);
ECHMET_API const char * ECHMET_CC NMFerrorToString(const RetCode err);
ECHMET_API void ECHMET_CC releaseInitialization(Initialization &init);
ECHMET_API void ECHMET_CC releaseProfileSlice(ProfileSlice &slice);
ECHMET_API const char * ECHMET_CC simFailToString(const SimulationFailure fail);
ECHMET_API const char * ECHMET_CC startFailToString(const StartFailure fail);
ECHMET_API const char * ECHMET_CC versionString();

} // extern "C"

inline
RetCode makeProfiles(const std::vector<std::vector<double>> &space,
		     Profiles *&profiles, NMF::InitializationFailure &fail)
{
	const auto size = space.size();

	if (size < 1) {
		fail = NMF::InitializationFailure::NO_CONSTITUENTS;
		return RetCode::E_INVALID_ARGUMENT;
	}

	RetCode tRet{};
	auto concs = new ECHMET::RealVec *[size];

	/* Allow safe error handling */
	for (size_t idx{0}; idx < size; idx++)
		concs[idx] = nullptr;
	for (size_t idx{0}; idx < size; idx++) {
		concs[idx] = ECHMET::createRealVec(size);
		if (concs[idx] == nullptr)
			goto fail;
	}

	/* Fill the array */
	for (size_t idx{0}; idx < size; idx++) {
		auto &iv = space.at(idx);
		auto v = concs[idx];
		if (v->resize(iv.size()) != ECHMET::RetCode::OK)
			goto fail;

		for (size_t jdx{0}; jdx < iv.size(); jdx++)
			(*v)[jdx] = iv.at(jdx);
	}

	/* Check that all input vector have the same size */
	for (size_t idx{1}; idx < size; idx++) {
		if (concs[idx - 1]->size() != concs[idx]->size()) {
			tRet = RetCode::E_INVALID_ARGUMENT;
			fail = InitializationFailure::MISMATCHING_PROFILES;
			goto fail;
		}
	}

	tRet = makeProfiles(concs, size, profiles);

fail:
	for (size_t idx{0}; idx < size; idx++) {
		auto v = concs[idx];
		if (v != nullptr)
			v->destroy();
	}

	delete [] concs;

	return tRet;
}

} // namespace NMF

#endif // LIBNMF_H


